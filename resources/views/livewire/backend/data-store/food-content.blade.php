<div>
    {{-- ======================================== name page ====================================================== --}}
    <div class="right_col" role="main">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h6><i class="fa fa-database"></i> ຈັດການຂໍ້ມູນ <i class="fa fa-angle-double-right"></i>
                            ເມນູອາຫານ</h6>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">ຫນ້າຫຼັກ</a></li>
                            <li class="breadcrumb-item active">ເມນູອາຫານ</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    {{-- ======================================== show and seach data ====================================================== --}}
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header bg-pink">
                                <div class="row">
                                    <div class="col-md-8">
                                    </div>
                                    <div class="input-group input-group-sm" style="width: 200px;">
                                        <input wire:model="search" type="text" class="form-control"
                                            placeholder="ຄົ້ນຫາ...">

                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-borderless">
                                        <thead class="bg-light">
                                            <tr class="text-center">
                                                <th>ລຳດັບ</th>
                                                <th>ລະຫັດ</th>
                                                <th>ປະເພດອາຫານ</th>
                                                <th>ຮູບພາບ</th>
                                                <th>ຊື່</th>
                                                <th>ລາຄາ</th>
                                                <th>ສະຖານະ</th>
                                                @foreach ($rolepermissions as $items)
                                                @if ($items->permissionname->name == 'action_food')
                                                <th>ຈັດການ</th>
                                                @endif
                                            @endforeach
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                                $num = 1;
                                            @endphp
                                            @foreach ($foods as $item)
                                                <tr class="text-center">
                                                    <td>{{ $num++ }}</td>
                                                    <td>{{ $item->code }}</td>
                                                    @if (!empty($item->foods_type))
                                                        <td>{{ $item->foods_type->name }}</td>
                                                    @else
                                                        <td></td>
                                                    @endif
                                                    <td>
                                                        @if (!empty($item->image))
                                                            <a href="{{ asset('public/'.$item->image) }}">
                                                                <img class="rounded" src="{{ asset('public/'.$item->image) }}"
                                                                    width="60px;" height="60px;">
                                                            </a>
                                                        @else
                                                            <img src="{{ 'logo/noimage.jpg' }}" width="60px;"
                                                                height="60px;">
                                                        @endif
                                                    </td>
                                                    <td>{{ $item->name }}</td>
                                                    <td>{{ number_format($item->price) }}.00 ₭</td>
                                                    <td>
                                                        @if ($item->status == 0)
                                                            <button wire:click="CloseMenu({{ $item->id }})"
                                                                type="button"
                                                                class="btn btn-outline-danger btn-sm"><i class="fas fa-times-circle"></i> ເມນູປິດ</button>
                                                        @elseif($item->status == 1)
                                                            <button wire:click="OpenMenu({{ $item->id }})"
                                                                type="button"
                                                                class="btn btn-outline-success btn-sm"><i class="fas fa-check-circle"></i> ເມນູເປີດ</button>
                                                        @else
                                                        @endif
                                                    </td>
                                                    @foreach ($rolepermissions as $items)
                                                @if ($items->permissionname->name == 'action_food')
                                                    <td>
                                                        <div class="btn-group">
                                                            <button wire:click="edit({{ $item->id }})"
                                                                type="button" class="btn btn-warning btn-sm"><i
                                                                    class="fa fa-pencil"></i></button>
                                                            <button wire:click="showDestroy({{ $item->id }})"
                                                                type="button" class="btn btn-danger btn-sm"><i
                                                                    class="fa fa-trash"></i></button>
                                                        </div>
                                                        @endif
                                                        @endforeach
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <div class="float-right">
                                        {{ $foods->links() }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @include('livewire.backend.data-store.foods-create-update')
                </div>
            </div>
        </section>
        {{-- ======================================== modal-delete ====================================================== --}}
        <div class="modal fade" id="modal-delete">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-danger">
                        <h4 class="modal-title"><i class="fa fa-trash text-white"></i></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body text-center">
                        <input type="hidden" wire:model="ID">
                        <h4 class="text-center">ທ່ານຕ້ອງການລຶບ <b>({{ $code }})</b> ບໍ່?</h4>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">ຍົກເລີກ</button>
                        <button wire:click="destroy({{ $ID }})" type="button"
                            class="btn btn-success">ຕົກລົງ</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
