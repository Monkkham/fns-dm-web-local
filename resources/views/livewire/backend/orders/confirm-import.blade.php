<div wire:ignore.self id="modala" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-light">
                <h5 class="modal-title" id="myModalLabel"><i class="fa fa-cart-plus"></i>
                    ລາຍການສິນຄ້າທີ່ສັ່ງຊື້</h5>
                <button type="button" class="close text-white" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
                <div class="col-sm-3">
                    <div class="form-group clearfix">
                        <div class="icheck-success d-inline">
                            <input type="radio" id="radioPrimary1" value="1" wire:model="payment_type"
                                checked>
                            <label for="radioPrimary1"><span class="text-success">ເງິນສົດ</span>
                            </label>
                        </div>
                        {{-- </div>
                    <div class="form-group clearfix"> --}}
                        <div class="icheck-success d-inline">
                            <input type="radio" id="radioPrimary2" value="2" wire:model="payment_type">
                            <label for="radioPrimary2"><span class="text-danger">ເງິນໂອນ</span>
                            </label>
                        </div>
                    </div>
                    @error('gender')
                        <span style="color: red" class="error">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 col-sm-8">
                        <div class="x_panel">
                            <div class="x_content">
                                <section class="content invoice">
                                    <!-- title row -->
                                    {{-- <div class="row">
                                    <div class="invoice-header">
                                        <h4>
                                            <i class="fa fa-globe"></i>
                                            ຮ້ານ: ອາຫານດາວອັງຄານ
                                        </h4>
                                    </div>
                                    <!-- /.col -->
                                </div> --}}
                                    <!-- info row -->
                                    {{-- <div class="row invoice-info">
                                    <div class="col-sm-12 invoice-col">
                                        <b>ຜູ້ພິມບິນສັ່ງຊື້: {{ Auth::guard('admin')->user()->name }}</b>
                                        <br>
                                        <b>ວັນທີ່:</b> {{ date('d-m-Y') }}
                                    </div>
                                    <!-- /.col -->
                                </div> --}}
                                    <!-- Table row -->
                                    <div class="  table">
                                        <table class="table table-striped">
                                            <thead class="bg-pink text-white">
                                                <tr>
                                                    <th>ລຳດັບ</th>
                                                    <th>ສິນຄ້າ</th>
                                                    <th>ລາຄາ</th>
                                                    <th>ຈຳນວນ</th>
                                                    <th>ລວມເປັນເງິນ</th>
                                                    <th>ຈັດການ</th>
                                                </tr>
                                            </thead>
                                            @php
                                                $num = 1;
                                            @endphp
                                            <tbody>
                                                @if (count((array) $importOrderItems) > 0)
                                                    @foreach ($importOrderItems as $item)
                                                        <tr>
                                                            <td>{{ $num++ }}
                                                            </td>
                                                            <td>{{ $item->products->name }}
                                                            </td>

                                                            <td>{{ number_format($item->products->buy_price) }} ₭
                                                            <td>
                                                                <button
                                                                    wire:click="updateStock({{ $item->orders_id }}, {{ $item->product_id }}, {{ -1 }})"
                                                                    class="btn btn-primary btn-sm">
                                                                    <i class="fa fa-minus-square"></i>
                                                                </button>
                                                                {{ $item->quantity }}
                                                                <button
                                                                    wire:click="updateStock({{ $item->orders_id }}, {{ $item->product_id }}, {{ 1 }})"
                                                                    class="btn btn-primary btn-sm">
                                                                    <i class="fa fa-plus-square"></i>
                                                                </button>
                                                            </td>
                                                            </td>
                                                            <td>{{ number_format($item->subtotal) }} ₭
                                                            </td>
                                                            <td>
                                                                <button
                                                                    wire:click="DeleteOrderItem({{ $item->id }})"
                                                                    type="button" class="btn btn-danger btn-sm"><i
                                                                        class="fas fa-trash"></i></button>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @else
                                                    <tr>
                                                        <td colspan="4" class="text-center">
                                                            ບໍ່ມີລາຍການ</td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.col -->
                                    {{-- </div> --}}
                                    <!-- /.row -->

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <tbody>
                                                        <tr>
                                                            <th>
                                                                <h6 class="text-bold">ລວມຈຳນວນ:
                                                                </h6>
                                                            </th>
                                                            <td>
                                                                <h6 class="text-bold">
                                                                    {{ $this->count_product }} ລາຍການ
                                                                </h6>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th>
                                                                <h5 class="text-bold">
                                                                    ລວມເປັນເງິນ:</h5>
                                                            </th>
                                                            <td>
                                                                <h5 class="text-bold">
                                                                    {{ $this->subtotal }} ₭</h5>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!-- /.col -->
                                    </div>
                            </div>
                        </div>
                    </div>
                    {{-- <div class="col-md-5 col-sm-4">
                    <form class="form-horizontal">
                        <div class="form-group row">
                            <select wire:model='supplier_id' id="suplyerId"
                                class="form-control">
                                <option selected>----- ເລືອກຜູ້ສະຫນອງ -----</option>
                                @foreach ($suppliers as $item)
                                    <option value="{{ $item->id }}">
                                        {{ $item->name }} {{ $item->phone }}
                                    </option>
                                @endforeach
                            </select>
                            @error('supplier_id')
                                <span style="color: red"
                                    class="error">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class=" table">
                            <table class="table table-striped">
                                <thead>
                                    @if ($suppiler_data)
                                        @if (!empty($suppiler_data))
                                            <tr>
                                                <th class="bg-light">ຊື່ ນາມສະກຸນ:</th>
                                                <th>{{ $suppiler_data->name }}
                                                    {{ $suppiler_data->lastname }}</th>
                                            </tr>
                                            <tr>
                                                <th class="bg-light">ເພດ:</th>
                                                <th>
                                                    @if ($suppiler_data->gender == 1)
                                                        <span>ຍິງ</span>
                                                    @elseif($suppiler_data->gender == 2)
                                                        <span>ຊາຍ</span>
                                                    @endif
                                                </th>
                                            </tr>
                                            <tr>
                                                <th class="bg-light">ເບີໂທ:</th>
                                                <th>{{ $suppiler_data->phone }}</th>
                                            </tr>
                                            <tr>
                                                <th class="bg-light">ອີເມວ:</th>
                                                <th>{{ $suppiler_data->email }}</th>
                                            </tr>
                                            <tr>
                                                <th class="bg-light">ທີ່ຢູ່:</th>
                                                <th>
                                                    {{ $suppiler_data->village->name_la }},
                                                    {{ $suppiler_data->district->name_la }},
                                                    {{ $suppiler_data->province->name_la }}
                                                </th>
                                            </tr>
                                        @endif
                                    @endif
                                </thead>
                            </table>
                        </div>
                </div> --}}
                </div>
            </div>
            <div class="modal-footer justify-content-between bg-light">
                <button wire:click='icomeOk({{ $orderId }},{{ 2 }})' type="button" class="btn btn-danger"><i class="fas fa-times-circle"></i>
                    ຍົກເລີກນຳເຂົ້າ</button>
                <button wire:click='icomeOk({{ $orderId }},{{ 3 }})' type="button" class="btn btn-success"><i
                        class="fas fa-check-circle"></i> ຢືນຍັນນຳເຂົ້າ</button>
            </div>
</div>
</div>
</div>
