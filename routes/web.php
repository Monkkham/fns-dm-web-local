<?php

use Illuminate\Support\Facades\Route;
use App\Http\Livewire\Backend\RoleContent;
use App\Http\Livewire\Backend\LoginContent;
use App\Http\Livewire\Backend\UsersContent;
use App\Http\Livewire\Backend\LogoutContent;
use App\Http\Livewire\Backend\RoleEditContent;
use App\Http\Livewire\Backend\DashboardContent;
use App\Http\Livewire\Backend\RoleCreateContent;
use App\Http\Livewire\Backend\Store\PostContent;
use App\Http\Livewire\Backend\Store\AboutContent;
use App\Http\Livewire\Backend\Store\SlideContent;
use App\Http\Livewire\Backend\Orders\ImportContent;
use App\Http\Livewire\Backend\Orders\OrdersContent;
use App\Http\Livewire\Backend\DataStore\FoodContent;
use App\Http\Livewire\Backend\DataStore\UnitsContent;
use App\Http\Livewire\Backend\DataStore\ZonesContent;
use App\Http\Livewire\Backend\Service\ServiceContent;
use App\Http\Livewire\Backend\DataStore\TablesContent;
use App\Http\Livewire\Backend\DataStore\ProductContent;
use App\Http\Livewire\Backend\Orders\OrderPrintContent;
use App\Http\Livewire\Backend\DataStore\CustomerContent;
use App\Http\Livewire\Backend\DataStore\EmployeeContent;
use App\Http\Livewire\Backend\DataStore\FoodTypeContent;
use App\Http\Livewire\Backend\DataStore\NotebookContent;
use App\Http\Livewire\Backend\DataStore\SupplierContent;
use App\Http\Livewire\Backend\PreOrders\PreOrdersContent;
use App\Http\Livewire\Backend\Reports\OrderReportContent;
use App\Http\Livewire\Backend\Reports\SalesReportContent;
use App\Http\Livewire\Backend\Reports\ExpendReportContent;
use App\Http\Livewire\Backend\Reports\IncomeReportContent;
use App\Http\Livewire\Backend\DataStore\ProductTypeContent;
use App\Http\Livewire\Backend\Reports\ProductReportContent;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// ==================== Route Frontend ====================== //
//     Route::get('/register', RegisterContent::class)->name('frontend.register');
//     Route::get('/login', App\Http\Livewire\Frontend\LoginContent::class)->name('frontend.login');
//     Route::get('/', HomeContent::class)->name('home');
//     Route::get('/about-us', AboutUsContent::class)->name('frontend.about');
//     Route::get('/contact-us', ContactContent::class)->name('frontend.contact');
// Route::middleware('auth.frontend:web')->group(function () {
//     Route::get('/profiles/{id}', ProfileContent::class)->name('frontend.profile');
//     Route::get('/cusbuylands', CusBuyLandContent::class)->name('frontend.cusbuyland');
//     Route::get('/payinstallments/{slug_id}', PayInstallment::class)->name('frontend.payinstallment');
//     Route::get('/customer-logout', [App\Http\Livewire\Frontend\LoginContent::class, 'logout'])->name('frontend.logout');
// });

// ======================= Route Backend ================= //
Route::get('/', LoginContent::class)->name('login');
Route::group(['middleware' => 'auth.backend:admin'], function () {
    Route::get('/logout', [LogoutContent::class, 'logout'])->name('logout');
    Route::get('/admin/profiles/{id}', App\Http\Livewire\Backend\ProfileContent::class)->name('backend.profile');
    Route::get('/dashboard', DashboardContent::class)->name('dashboard');
    // =========== Datastore ========== //
    Route::get('/employees', EmployeeContent::class)->name('backend.employee');
    Route::get('/customers', CustomerContent::class)->name('backend.customer');
    Route::get('/suppliers', SupplierContent::class)->name('backend.supplier');
    Route::get('/products', ProductContent::class)->name('backend.product');
    Route::get('/product-type', ProductTypeContent::class)->name('backend.productType');
    Route::get('/units', UnitsContent::class)->name('backend.unit');
    Route::get('/food-type', FoodTypeContent::class)->name('backend.foodType');
    Route::get('/zones', ZonesContent::class)->name('backend.zone');
    Route::get('/tables', TablesContent::class)->name('backend.table');
    Route::get('/foods', FoodContent::class)->name('backend.food');
    Route::get('/notebooks', NotebookContent::class)->name('backend.notebook');
    // =========== orders ========== //
    Route::get('/orders', OrdersContent::class)->name('backend.order');
    Route::get('/order-print-bill/{slug_id}', OrderPrintContent::class)->name('backend.printbill');
    Route::get('/imports', ImportContent::class)->name('backend.import');
    // =========== preorders ========== //
    Route::get('/preorders', PreOrdersContent::class)->name('backend.preorder');
    Route::get('/services', ServiceContent::class)->name('backend.service');
    // ======================= reports ===================== //
    Route::get('/report-orders', OrderReportContent::class)->name('backend.report-order');
    Route::get('/report-sales', SalesReportContent::class)->name('backend.report-sale');
    Route::get('/report-products', ProductReportContent::class)->name('backend.report-product');
    Route::get('/report-incomes', IncomeReportContent::class)->name('backend.report-income');
    Route::get('/report-expends', ExpendReportContent::class)->name('backend.report-expend');
    // ======================= ກ່ຽວກັບອົງກອນ ===================== //
    Route::get('/abouts', AboutContent::class)->name('backend.about');
    Route::get('/slides', SlideContent::class)->name('backend.slide');
    Route::get('/post-public', PostContent::class)->name('backend.post');
    // ======================= ຜູ້ໃຊ້ ສິດທິນຳໃຊ້ລະບົບ ===================== //
    Route::get('/users', UsersContent::class)->name('backend.user');
    Route::get('/roles', RoleContent::class)->name('backend.role');
    Route::get('/create-roles', RoleCreateContent::class)->name('backend.create_role');
    Route::get('/edit-roles/{id}', RoleEditContent::class)->name('backend.edit_role');
});