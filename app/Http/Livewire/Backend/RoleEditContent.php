<?php

namespace App\Http\Livewire\Backend;

use App\Models\Roles;
use Livewire\Component;
use App\Models\RolePermission;

class RoleEditContent extends Component
{
    public $ID;
    public $name, $meaning, $settings, $reports, $users;
    public $arr = [];
    public $selectedtypes = [];
    public function mount($id)
    {
        $this->ID = $id;
        $role  = Roles::find($id);
        $roleandpermission = RolePermission::select('permissions_id')->where('role_id', $id)->get();
        $this->name = $role->name;
        $this->meaning = $role->meaning;
        foreach($roleandpermission as $item){
            $this->arr = array_push($this->selectedtypes,$item->permissions_id);
        }
        $this->settings = $role->settings;
        $this->reports = $role->reports;
        $this->users = $role->users;
        // dd($this->selectedtypes);
    }
    public function render()
    {
        return view('livewire.backend.role-edit-content')->layout('layouts.backend.base');
    }
    public function update()
    {
        $this->validate([
            'name'=>'required'
        ],[
            'name.required'=>'ກະລຸນາເພີ່ມຊື່ສິດການເຂົ້າເຖິງກ່ອນ!'
        ]);

        if(!empty($this->selectedtypes)){
            $role = Roles::find($this->ID);
            $role->name = $this->name;
            $role->meaning = $this->meaning;
            if($this->settings == 'true'){
                $role->settings = $this->settings;
            }
            if($this->reports == 'true'){
                $role->reports = $this->reports;
            }
            if($this->users == 'true'){
                $role->users = $this->users;
            }
            $role->save();

            $permis = RolePermission::where('role_id', $this->ID)->delete();

            foreach ($this->selectedtypes as $item)
            {
                $role_permis = new RolePermission();
                $role_permis->role_id = $role->id;
                $role_permis->permissions_id = $item;
                $role_permis->save();
            }
            session()->flash('success', 'ແກ້ໄຂຂໍ້ມູນສຳເລັດ');
            return redirect(route('backend.role'));
        }else{
            $this->emit('alert', ['type' => 'warning', 'message' => 'ກະລຸນາເລືອກຂໍ້ມູນກ່ອນ!']);
        }
    }

    public function back()
    {
        return redirect(route('backend.role'));
    }
}
