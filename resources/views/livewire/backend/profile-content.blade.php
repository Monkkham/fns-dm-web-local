<div>
    {{-- ======================================== header page ====================================================== --}}
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h4><i class="fas fa-user-tie"></i> ໂປຣຟາຍຜູ້ໃຊ້ </h4>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">ຫນ້າຫຼັກ</a></li>
                        <li class="breadcrumb-item active">ໂປຣຟາຍຜູ້ໃຊ້</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-7">
                    <!-- Profile Image -->
                    <div class="card card-primary card-outline">
                        <div class="card-body box-profile">
                            <div class="text-center">
                                @if (!empty(Auth::guard('admin')->user()->image))
                                    <img class="profile-user-img img-fluid img-circle"
                                        src="{{ asset('upload/employee') }}/{{ Auth::guard('admin')->user()->image }}"
                                        alt="User profile picture">
                                @else
                                    <img class="profile-user-img img-fluid img-circle"
                                        src="{{ asset('logo/noimage.png') }}" alt="User profile picture">
                                @endif
                            </div>
                            <h3 class="profile-username text-center">{{ Auth::guard('admin')->user()->name }}</h3>

                            <p class="text-muted text-center">{{ Auth::guard('admin')->user()->lastname }}</p>

                            <ul class="list-group list-group-unbordered mb-3">
                                <li class="list-group-item">
                                    <b>ລະຫັດ: </b> <a class="float-center">{{ Auth::guard('admin')->user()->code }}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>ສິດທິເຂົ້າໃຊ້: </b> <a class="float-center">
                                        @if (!empty(Auth::guard('admin')->user()->roles))
                                            {{ Auth::guard('admin')->user()->roles->name }}
                                        @endif
                                    </a>
                                </li>
                                <li class="list-group-item">
                                    <b>ເພດ: </b>
                                    @if (Auth::guard('admin')->user()->gender == 1)
                                        <a class="float-center">ຍິງ</a>
                                    @elseif(Auth::guard('admin')->user()->gender == 2)
                                        <a class="float-center">ຊາຍ</a>
                                    @endif
                                </li>
                                <li class="list-group-item">
                                    <b>ສະຖານະ: </b> <a class="float-center">
                                        @if (Auth::guard('admin')->user()->status == 1)
                                            <a class="float-center">ໂສດ</a>
                                        @elseif(Auth::guard('admin')->user()->status == 2)
                                            <a class="float-center">ມີເເຟນ</a>
                                        @elseif(Auth::guard('admin')->user()->status == 3)
                                            <a class="float-center">ແຕ່ງງານ</a>
                                        @endif
                                </li>
                                <li class="list-group-item">
                                    <b>ທີ່ຢູ່: </b> <a class="float-center">
                                        @if(!empty(Auth::guard('admin')->user()->village))
                                        {{ Auth::guard('admin')->user()->village->name_la }},
                                        {{ Auth::guard('admin')->user()->district->name_la }},
                                        {{ Auth::guard('admin')->user()->province->name_la }}
                                        @endif
                                </li>
                            </ul>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="tab-content">
                                <div class="active tab-pane" id="settings">
                                    <form class="form-horizontal">
                                        <div class="form-group row">
                                            <label for="inputName" class="col-sm-4 col-form-label">ຮູບພາບ</label>
                                            <div class="col-sm-12">
                                                <input type="file" wire:model="image" accept=".png, .jpg, .jpeg"
                                                    class="form-control" id="inputName" placeholder="ປ້ອນຂໍ້ມູນ">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputName" class="col-sm-4 col-form-label">ຊື່</label>
                                            <div class="col-sm-12">
                                                <input type="text" wire:model="name" class="form-control"
                                                    id="inputName" placeholder="ປ້ອນຂໍ້ມູນ">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputName" class="col-sm-4 col-form-label">ນາມສະກຸນ</label>
                                            <div class="col-sm-12">
                                                <input type="text" wire:model="lastname" class="form-control"
                                                    id="inputName" placeholder="ປ້ອນຂໍ້ມູນ">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputName" class="col-sm-4 col-form-label">ເບີໂທ</label>
                                            <div class="col-sm-12">
                                                <input type="tel" wire:model="phone" class="form-control"
                                                    id="inputName" placeholder="ປ້ອນຂໍ້ມູນ">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputEmail" class="col-sm-4 col-form-label">ອີເມວ</label>
                                            <div class="col-sm-12">
                                                <input type="email" wire:model="email" class="form-control"
                                                    id="inputEmail" placeholder="ປ້ອນຂໍ້ມູນ">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputName2"
                                                class="col-sm-4 col-form-label">ລະຫັດຜ່ານເກົ່າ</label>
                                            <div class="col-sm-12">
                                                <input type="text" wire:model="old_password"
                                                    class="form-control @error('password') is-invalid @enderror"
                                                    id="inputName2" placeholder="ປ້ອນຂໍ້ມູນ">
                                                @error('old_password')
                                                    <span style="color: red" class="error">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputName2"
                                                class="col-sm-4 col-form-label">ລະຫັດຜ່ານໃຫມ່</label>
                                            <div class="col-sm-12">
                                                <input type="text" wire:model="password"
                                                    class="form-control @error('password') is-invalid @enderror"
                                                    id="inputName2" placeholder="ປ້ອນຂໍ້ມູນ">
                                                @error('password')
                                                    <span style="color: red" class="error">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputName2"
                                                class="col-sm-4 col-form-label">ຍືນຍັນລະຫັດ</label>
                                            <div class="col-sm-12">
                                                <input type="text" wire:model="confirmpassword"
                                                    class="form-control @error('confirmpassword') is-invalid @enderror"
                                                    id="inputName2" placeholder="ປ້ອນຂໍ້ມູນ">
                                                @error('confirmpassword')
                                                    <span style="color: red" class="error">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="offset-sm-4 col-sm-12">
                                                <button type="button" wire:click="updateProfile"
                                                    class="btn btn-warning"><i class="fas fa-edit"></i>
                                                    ອັບເດດໂປຣຟາຍ</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.tab-pane -->
                            </div>
                            <!-- /.tab-content -->
                        </div><!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
