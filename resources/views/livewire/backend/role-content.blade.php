<div>
    {{-- ======================================== name page ====================================================== --}}
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h6><i class="fas fa-user-tie"></i> ຜູ້ໃຊ້&ສິດທິ <i class="fa fa-angle-double-right"></i>
                        ສິດທິເຂົ້າເຖິງ</h6>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">ຫນ້າຫຼັກ</a></li>
                        <li class="breadcrumb-item active">ສິດທິເຂົ້າເຖິງ</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-9">
                                    @foreach ($rolepermissions as $items)
                                        @if ($items->permissionname->name == 'action_role')
                                            <a href="{{ route('backend.create_role') }}"
                                                class="btn btn-primary btn-sm"><i class="fas fa-plus-circle"></i>
                                                ເພີ່ມສິດທິໃຫມ່</a>
                                        @endif
                                    @endforeach
                                </div>
                                <div class="col-md-3">
                                    <input type="text" wire:model="search" class="form-control" placeholder="ຄົ້ນຫາ">
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <table class="table table-bordered">
                                <thead>
                                    <tr class="text-center bg-pink">
                                        <th>ລຳດັບ</th>
                                        <th>ຊື່</th>
                                        <th>ຄວາມຫມາຍ</th>
                                        @foreach ($rolepermissions as $items)
                                            @if ($items->permissionname->name == 'action_role')
                                                <th>ຈັດການ</th>
                                            @endif
                                        @endforeach
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $no = 1; @endphp
                                </tbody>
                                <tfoot>
                                    @foreach ($roles as $item)
                                        <tr class="text-center">
                                            <td>{{ $no++ }}</td>
                                            <td>{{ $item->name }}</td>
                                            <td>
                                                {{ $item->meaning }}
                                            </td>
                                            @foreach ($rolepermissions as $items)
                                                @if ($items->permissionname->name == 'action_role')
                                            <td>
                                                <div class="btn-group">
                                                    <button wire:click="edit({{ $item->id }})" type="button"
                                                        class="btn btn-warning btn-sm"><i
                                                            class="fas fa-pen-alt"></i></button>
                                                    <button wire:click="showDestroy({{ $item->id }})" type="button"
                                                        class="btn btn-danger btn-sm"><i
                                                            class="fas fa-trash-alt"></i></button>
                                                </div>

                                            </td>
                                            @endif
                                            @endforeach
                                        </tr>
                                    @endforeach
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row (main row) -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->

    <!-- /.modal-delete -->
    <div class="modal fade" id="modal-delete">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h4 class="modal-title"><i class="fa fa-trash"></i></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" wire:model="ID">
                    <h3 class="text-center">ຕ້ອງການລຶບສິດທິອອກບໍ່</h3>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">ຍົກເລີກ</button>
                    <button wire:click="destroy({{ $ID }})" type="button"
                        class="btn btn-success">ຕົກລົງ</button>
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        window.addEventListener('show-delete', event => {
            $('#modal-delete').modal('show');
        })
        window.addEventListener('hide-delete', event => {
            $('#modal-delete').modal('hide');
        })
    </script>
@endpush
