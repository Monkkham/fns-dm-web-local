<div>
    {{-- ======================================== name page ====================================================== --}}
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h6><i class="fas fa-users"></i> ຜູ້ໃຊ້&ສິດທິ <i class="fa fa-angle-double-right"></i>
                        ສິດທິເຂົ້າເຖິງ <i class="fa fa-angle-double-right"></i> ແກ້ໄຂ</h6>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">ຫນ້າຫຼັກ</a></li>
                        <li class="breadcrumb-item active"> ແກ້ໄຂ</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>ຊື່ສິດທິ</label>
                                        <input wire:model="name" type="text"
                                            class="form-control @error('name') is-invalid @enderror"
                                            placeholder="ປ້ອນຂໍ້ມູນ">
                                        @error('name')
                                            <span style="color: red" class="error">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>ຄວາມຫມາຍ</label>
                                        <input wire:model="meaning" type="text"
                                            class="form-control @error('meaning') is-invalid @enderror"
                                            placeholder="ປ້ອນຂໍ້ມູນ">
                                        @error('meaning')
                                            <span style="color: red" class="error">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <!-- ========================= Start =========================== -->
                                <div class="col-md-2">
                                    <h6 class="bg-pink p-1"><i class="fas fa-shield-alt"></i> ຈັດການຂໍ້ມູນ</h6>
                                </div>
                                <div class="col-md-10">
                                    <hr>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-10">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="1">
                                                <label for="name" style="color:black">ພະນັກງານ</label>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="2">
                                                <label for="name" style="color:black">ຜູ້ສະຫນອງ</label>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="3">
                                                <label for="name" style="color:black">ລູກຄ້າ</label>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="4">
                                                <label for="name" style="color:black">ສີນຄ້າ</label>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="5">
                                                <label for="name" style="color:black">ປະເພດສິນຄ້າ</label>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="6">
                                                <label for="name" style="color:black">ເມນູອາຫານ</label>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="7">
                                                <label for="name" style="color:black">ປະເພດອາຫານ</label>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="8">
                                                <label for="name" style="color:black">ໂຊນຮ້ານ</label>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="9">
                                                <label for="name" style="color:black">ໂຕະຕັ່ງ</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                            <!-- ========================= Start =========================== -->
                            <div class="row">
                                <div class="col-md-2">
                                    <h6 class="bg-pink p-1"><i class="fas fa-shield-alt"></i> ສັ່ງຊື້-ສັ່ງຈອງ
                                    </h6>
                                </div>
                                <div class="col-md-10">
                                    <hr>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-10">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="10">
                                                <label for="name" style="color:black">ສັ່ງຊື້ສິນຄ້າ</label>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="11">
                                                <label for="name" style="color:black">ນຳເຂົ້າສິນຄ້າ</label>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="12">
                                                <label for="name" style="color:black">ສັ່ງຈອງ</label>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                -
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                -
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                -
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                            <!-- ========================= Start =========================== -->
                            <div class="row">
                                <div class="col-md-2">
                                    <h6 class="bg-pink p-1"><i class="fas fa-shield-alt"></i> ບໍລິການ
                                    </h6>
                                </div>
                                <div class="col-md-10">
                                    <hr>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-10">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="25">
                                                <label for="name" style="color:black">ປຸ່ມຮັບຄົວກິນ</label>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="26">
                                                <label for="name" style="color:black">ປຸ່ມຮ້ອງເສີບ</label>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="27">
                                                <label for="name" style="color:black">ປຸ່ມສົ່ງບິນ</label>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="28">
                                                <label for="name" style="color:black">ປຸ່ມຊຳລະເງິນ</label>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                -
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                -
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                            </div>

                            <!-- ========================= Start =========================== -->
                            <div class="row">
                                <div class="col-md-2">
                                    <h6 class="bg-pink p-1"><i class="fas fa-shield-alt"></i> ລາຍງານ</h6>
                                </div>
                                <div class="col-md-10">
                                    <hr>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-10">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="14">
                                                <label for="name" style="color:black">ລາຍງານການຊື້</label>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="15">
                                                <label for="name" style="color:black">ລາຍງານການຂາຍ</label>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="16">
                                                <label for="name" style="color:black">ລາຍງານຂໍ້ມູນສິນຄ້າ</label>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="17">
                                                <label for="name" style="color:black">ລາຍງານລາຍຮັບ</label>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="18">
                                                <label for="name" style="color:black">ລາຍງານລາຍຈ່າຍ</label>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                -
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                            <!-- ========================= Start =========================== -->
                            <div class="row">

                                <div class="col-md-2">
                                    <h6 class="bg-pink p-1"><i class="fas fa-shield-alt"></i> ຜູ້ໃຊ້ & ສິດທິ</h6>
                                </div>
                                <div class="col-md-10">
                                    <hr>
                                </div>
                            </div>
                            {{-- @if ($users == 'true') --}}
                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-10">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="19">
                                                <label for="name">ຜູ້ໃຊ້ລະບົບ</label>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <input wire:model="selectedtypes" type="checkbox" value="20">
                                                <label for="name">ສິດທິການເຂົ້າເຖິງ</label>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                -
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                -
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                -
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                -
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                            {{-- @endif --}}

                            <!-- ========================= Start =========================== -->
                            <div class="row">
                                <div class="col-md-2">

                                    <h6 class="bg-pink p-1"><i class="fas fa-shield-alt"></i> ຕັ້ງຄ່າ-ບັນທຶກປະຈຳວັນ
                                    </h6>
                                </div>
                                <input wire:model="users" type="checkbox" value="true"> ເພີ່ມເຕີມ
                                <div class="col-md-10">
                                    <hr>
                                </div>
                            </div>
                            @if ($users == 'true')
                                <div class="row">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <input wire:model="selectedtypes" type="checkbox" value="21">
                                                    <label for="name">ກ່ຽວກັບ</label>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <input wire:model="selectedtypes" type="checkbox" value="22">
                                                    <label for="name">ສະໄລຮູບພາບ</label>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <input wire:model="selectedtypes" type="checkbox" value="23">
                                                    <label for="name">ໂພດສາທາລະນະ</label>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <input wire:model="selectedtypes" type="checkbox" value="24">
                                                    <label for="name">ບັນທຶກປະຈຳວັນ</label>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    -
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    -
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2"></div>
                                </div>
                            @endif
                        </div>
                        <div class="card-footer">
                            <div class="d-flex justify-content-between md-2">
                                <button type="button" wire:click="back" class="btn btn-warning"><i
                                        class="fas fa-reply-all"></i> ກັບຄືນ</button>
                                <button type="button" wire:click="update" class="btn btn-success"><i
                                        class="fas fa-save"></i> ບັນທຶກ</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
