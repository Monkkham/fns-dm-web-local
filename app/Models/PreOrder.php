<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PreOrder extends Model
{
    use HasFactory;
    protected $table = "preorder";
    protected $fillable = ['id',
    'customer_id',
    'code',
    'qtyp',
    'total',
    'payment', // 1 = ລໍຖ້າຍືນຍັນ, 0 = ຊຳລະເເລ້ວ
    'onepay_image',
    'status', 
    // 1 = ລໍຖ້າກວດສອບ (ສັ່ງຈອງມາໃຫມ່ກວດບິນຈ່າຍເງິນ)
    // 2 = ອານຸມັດເເລ້ວ (ຜ່ານການກວດສອບເເລ້ວ)
    // 3 = ບໍລິການສຳເລັດ (ລູກຄ້າເຊັກບິນອອກຮ້ານເເລ້ວ)
    // 0 = ຍົກເລີກບິນ
    'datetime',
    'created_at',
    'updated_at'];
    public function customer()
    {
        return $this->belongsTo('App\Models\Customer','customer_id','id');
    }
}
