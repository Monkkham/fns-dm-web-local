            <!--Foram add new-->
            <div class="col-md-4">
                <div class="card card-default">
                  <div class="card-header bg-pink">
                    <label>ເພີ່ມໃຫມ່ / ແກ້ໄຂ</label>
                  </div>
                  <form>
                      <div class="card-body">
                          <input type="hidden" wire:model="ID" value="{{$ID}}">
                          <div class="row">
                            <div class="col-md-12">
                              <div class="form-group" wire:ignore>
                                  <label>ເລືອກໂຊນຮ້ານ</label>
                                  <select wire:model="zones_id" class="form-control" id="select_fill">
                                    <option value="" selected>ເລືອກ</option>
                                      @foreach($zones as $item)
                                          <option value="{{ $item->id }}">ໂຊນ: {{ $item->name }}</option>
                                      @endforeach
                                  </select>
                                  @error('zones_id') <span style="color: red" class="error">{{ $message }}</span> @enderror
                              </div>
                          </div>
                              {{-- <div class="col-md-12">
                                <div class="form-group">
                                  <label>ຮູບພາບ</label>
                                  <input wire:model="image" type="file" class="form-control @error('image') is-invalid @enderror" placeholder="ປ້ອນຂໍ້ມູນ">
                                  @error('image') <span style="color: red" class="error">{{ $message }}</span> @enderror
                                </div>
                              </div> --}}
                              <div class="col-md-12">
                                <div class="form-group">
                                  <label>ຈຳນວນຕັ່ງ</label>
                                  <input wire:model="chiar_qty" type="text" class="form-control @error('chiar_qty') is-invalid @enderror" placeholder="ປ້ອນຂໍ້ມູນ">
                                  @error('chiar_qty') <span style="color: red" class="error">{{ $message }}</span> @enderror
                                </div>
                              </div>
                          </div>
                      </div>
                      <div class="card-footer">
                          <div class="d-flex justify-content-between md-2">
                            @foreach ($rolepermissions as $items)
                            @if ($items->permissionname->name == 'action_table')
                                  <button type="button" wire:click="resetform" class="btn btn-warning">ຣີເຊັດ</button>
                                  <button type="button" wire:click="store" class="btn btn-success">ບັນທຶກ</button>
                                  @endif
                              @endforeach
                          </div>
                      </div>
                  </form>
                </div>
              </div>

@push('scripts')
    <script>
      window.addEventListener('show-modal-delete', event => {
          $('#modal-delete').modal('show');
      })
      window.addEventListener('hide-modal-delete', event => {
          $('#modal-delete').modal('hide');
      })
    </script>
@endpush