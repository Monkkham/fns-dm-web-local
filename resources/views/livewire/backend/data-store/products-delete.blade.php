    <!-- /.modal-delete -->
    <div class="modal fade" id="modal-delete">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h4 class="modal-title"><i class="fa fa-trash text-white"></i></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <input type="hidden" wire:model="ID">
                    <h4 class="text-center">ທ່ານຕ້ອງການລຶບ <b>({{ $name }})</b> ບໍ່?</h4>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">ຍົກເລີກ</button>
                    <button wire:click="destroy({{ $ID }})" type="button"
                        class="btn btn-success">ຕົກລົງ</button>
                </div>
            </div>
        </div>
    </div>
    @push('scripts')
        <script>
            window.addEventListener('show-modal-add', event => {
                $('#modal-add').modal('show');
            })
            window.addEventListener('hide-modal-add', event => {
                $('#modal-add').modal('hide');
            })
            window.addEventListener('show-modal-edit', event => {
                $('#modal-edit').modal('show');
            })
            window.addEventListener('hide-modal-edit', event => {
                $('#modal-edit').modal('hide');
            })
            window.addEventListener('show-modal-delete', event => {
                $('#modal-delete').modal('show');
            })
            window.addEventListener('hide-modal-delete', event => {
                $('#modal-delete').modal('hide');
            })

            // ================= avatar image edit==========================
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#imagePreview').css('background-image', 'url(' + e.target.result + ')');
                        $('#imagePreview').hide();
                        $('#imagePreview').fadeIn(650);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $("#imageUpload").change(function() {
                readURL(this);
            });
            // ================ select2 ================ //
            $(document).ready(function() {
                $('#select1').select2();
                $('#select1').on('change', function(e) {
                    var data = $('#select1').select2("val");
                    @this.set('province_id', data);
                });
            });
            $(document).ready(function() {
                $('#select2').select2();
                $('#select2').on('change', function(e) {
                    var data = $('#select2').select2("val");
                    @this.set('province_id', data);
                });
            });
            // ================ summernote ================ //
            $('#note1').summernote({
                placeholder: 'ຄຳອະທິບາຍ',
                height: 150,
                callbacks: {
                    onChange: function(contents, $editable) {
                        @this.set('note1', contents);
                    }
                }
            });
            $('#note2').summernote({
                placeholder: 'ຄຳອະທິບາຍ',
                height: 150,
                callbacks: {
                    onChange: function(contents, $editable) {
                        @this.set('note2', contents);
                    }
                }
            });
        </script>
    @endpush
