<?php

namespace App\Http\Livewire\Backend;

use Livewire\Component;
use Illuminate\Support\Facades\Auth;
use App\Models\Employee;
use App\Models\Roles;
class LoginContent extends Component
{
    public $phone, $password, $remember,$name;
    public function mount()
    {
        config('auth.defaults.backend.guard');
    }
    public function render()
    {
        return view('livewire.backend.login-content')->layout('layouts.backend.login');
    }
    public function login()
    {
        $this->validate([
            'phone' => 'required',
            'password' => 'required',
        ],[
            'phone.required' => 'ກະລຸນາປ້ອນເບີໂທກ່ອນ!',
            'password.required' => 'ກະລຸນາປ້ອນລະຫັດຜ່ານກ່ອນ!',
        ]);
        if (Auth::guard('admin')->attempt([
            'phone' => $this->phone,
            'password' => $this->password],
            $this->remember)) 
        {
            if (Auth::guard('admin')->user()->roles->name === "Masterchef" || Auth::guard('admin')->user()->roles->name === "Employee"){
                session()->flash('success', 'ເຂົ້າສູ່ລະບົບສຳເລັດເເລ້ວ');
                return redirect(route('backend.service'));
            }else{
                session()->flash('success', 'ເຂົ້າສູ່ລະບົບສຳເລັດເເລ້ວ');
                return redirect(route('dashboard'));
            }
        }else{
            session()->flash('error', 'ເບີໂທ ຫລື ລະຫັດຜ່ານ ບໍ່ຖືກຕ້ອງ!ກະລຸນາລອງໃໝ່');
            return redirect(route('login'));
        }
    }
}
