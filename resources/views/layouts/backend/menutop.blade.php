{{-- <nav class="main-header navbar navbar-expand-md"> --}}
<nav class="main-header navbar navbar-expand-md navbar-light navbar-white">

    <div class="container-fluid">
        <a href="{{ route('dashboard') }}" class="navbar-brand">
            <img src="{{ asset('logo/logo.jpg') }}"
                alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
            <span class="brand-text font-weight-light">ຮ້ານ: ດາວອັງຄານ</span>
        </a>

        <!-- Left navbar links -->
        <div class="collapse navbar-collapse order-3" id="navbarCollapse">
            <ul class="navbar-nav">
                @if (empty(Auth::guard('admin')->user()->roles->name === 'Masterchef' ||
                            Auth::guard('admin')->user()->roles->name === 'Employee'
                    ))
                    {{-- ========== data store =========== --}}
                    <li class="nav-item dropdown">
                        <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false" class="nav-link dropdown-toggle"><i
                                class="fa fa-database text-pink"></i>
                            ຈັດການຂໍ້ມູນ</a>
                        <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
                            <li><a href="{{ route('backend.employee') }}" class="dropdown-item"><i
                                        class="fa fa-angle-double-right text-pink"></i>
                                    ພະນັກງານ</a></li>
                            <li class="dropdown-divider"></li>
                            <li><a href="{{ route('backend.supplier') }}" class="dropdown-item"><i
                                        class="fa fa-angle-double-right text-pink"></i>
                                    ຜູ້ສະຫນອງ</a></li>
                            <li class="dropdown-divider"></li>
                            <li><a href="{{ route('backend.customer') }}" class="dropdown-item"><i
                                        class="fa fa-angle-double-right text-pink"></i>
                                    ລູກຄ້າ</a></li>
                            <li class="dropdown-divider"></li>
                            <li><a href="{{ route('backend.product') }}" class="dropdown-item"><i
                                        class="fa fa-angle-double-right text-pink"></i>
                                    ສິນຄ້າ</a></li>
                            <li class="dropdown-divider"></li>
                            <li><a href="{{ route('backend.productType') }}" class="dropdown-item"><i
                                        class="fa fa-angle-double-right text-pink"></i>
                                    ປະເພດສິນຄ້າ</a></li>
                            <li class="dropdown-divider"></li>
                                    <li><a href="{{ route('backend.unit') }}" class="dropdown-item"><i
                                        class="fa fa-angle-double-right text-pink"></i>
                                    ຫົວຫນ່ວຍ</a></li>
                            <li class="dropdown-divider"></li>
                            <li><a href="{{ route('backend.food') }}" class="dropdown-item"><i
                                        class="fa fa-angle-double-right text-pink"></i>
                                    ເມນູອາຫານ</a></li>
                            <li class="dropdown-divider"></li>
                            <li><a href="{{ route('backend.foodType') }}" class="dropdown-item"><i
                                        class="fa fa-angle-double-right text-pink"></i>
                                    ປະເພດອາຫານ</a></li>
                            <li class="dropdown-divider"></li>
                            <li class="dropdown-divider"></li>
                            <li><a href="{{ route('backend.zone') }}" class="dropdown-item"><i
                                        class="fa fa-angle-double-right text-pink"></i>
                                    ໂຊນຮ້ານ</a></li>
                            <li class="dropdown-divider"></li>
                            <li><a href="{{ route('backend.table') }}" class="dropdown-item"><i
                                        class="fa fa-angle-double-right text-pink"></i>
                                    ໂຕະຕັ່ງ</a></li>
                            <li class="dropdown-divider"></li>
                            <li><a href="{{ route('backend.notebook') }}" class="dropdown-item"><i
                                        class="fa fa-angle-double-right text-pink"></i>
                                    ບັນທຶກປະຈຳວັນ</a></li>
                        </ul>
                    </li>
                    <li class="dropdown-divider"></li>
                    {{-- ========= orders ============ --}}
                    <li class="nav-item dropdown">
                        <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false" class="nav-link dropdown-toggle"><i
                                class="fas fa-cart-plus text-pink"></i>
                            ສັ່ງຊື້</a>
                        <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
                            <li><a href="{{ route('backend.order') }}" class="dropdown-item"><i
                                        class="fa fa-angle-double-right text-pink"></i>
                                    ສັ່ງຊື້ສິນຄ້າ</a></li>
                            <li class="dropdown-divider"></li>
                            <li><a href="{{ route('backend.import') }}" class="dropdown-item"><i
                                        class="fa fa-angle-double-right text-pink"></i>
                                    ນຳເຂົ້າສິນຄ້າ</a></li>
                        </ul>
                    </li>
                    <li class="dropdown-divider"></li>
                    {{-- ========= orders ============ --}}
                    <li class="nav-item dropdown">
                        <a id="dropdownSubMenu1" href="{{ route('backend.preorder') }}" aria-haspopup="true"
                            aria-expanded="false" class="nav-link"><i class="fas fa-users text-pink"></i>
                            ສັ່ງຈອງ</a>
                    </li>
                    <li class="dropdown-divider"></li>
                @endif
                {{-- ========= service ============ --}}
                <li class="nav-item dropdown">
                    <a id="dropdownSubMenu1" href="{{ route('backend.service') }}" aria-haspopup="true"
                        aria-expanded="false" class="nav-link"><i class="fas fa-store-alt text-pink"></i>
                        ບໍລິການ</a>
                </li>
                @if (empty(Auth::guard('admin')->user()->roles->name === 'Masterchef' ||
                            Auth::guard('admin')->user()->roles->name === 'Employee'
                    ))
                    <li class="dropdown-divider"></li>
                    {{-- ========= reports ============ --}}
                    <li class="nav-item dropdown">
                        <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false" class="nav-link dropdown-toggle"><i
                                class="fas fa-chart-line text-pink"></i>
                            ລາຍງານ</a>
                        <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
                            <li><a href="{{ route('backend.report-order') }}" class="dropdown-item"><i
                                        class="fa fa-angle-double-right text-pink"></i>
                                    ລາຍງານການຊື້</a></li>
                            <li class="dropdown-divider"></li>
                            <li><a href="{{ route('backend.report-sale') }}" class="dropdown-item"><i
                                        class="fa fa-angle-double-right text-pink"></i>
                                    ລາຍງານການຂາຍ</a></li>
                            <li class="dropdown-divider"></li>
                            <li><a href="{{ route('backend.report-product') }}" class="dropdown-item"><i
                                        class="fa fa-angle-double-right text-pink"></i>
                                    ລາຍງານຂໍ້ມູນສິນຄ້າ</a></li>
                            <li class="dropdown-divider"></li>
                            <li><a href="{{ route('backend.report-income') }}" class="dropdown-item"><i
                                        class="fa fa-angle-double-right text-pink"></i>
                                    ລາຍງານລາຍຮັບ</a></li>
                            <li class="dropdown-divider"></li>
                            <li><a href="{{ route('backend.report-expend') }}" class="dropdown-item"><i
                                        class="fa fa-angle-double-right text-pink"></i>
                                    ລາຍງານລາຍຈ່າຍ</a></li>
                        </ul>
                    </li>
                    <li class="dropdown-divider"></li>
                    {{-- ========= user & roles ============ --}}
                    <li class="dropdown-divider"></li>
                    <li class="nav-item dropdown">
                        <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false" class="nav-link dropdown-toggle"><i
                                class="fas fa-user-tie text-pink"></i>
                            ຜູ້ໃຊ້&ສິດທິ</a>
                        <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
                            <li><a href="{{ route('backend.user') }}" class="dropdown-item"><i
                                        class="fa fa-angle-double-right text-pink"></i>
                                    ຜູ້ໃຊ້ລະບົບ</a></li>
                            <li class="dropdown-divider"></li>
                            <li><a href="{{ route('backend.role') }}" class="dropdown-item"><i
                                        class="fa fa-angle-double-right text-pink"></i>
                                    ສິດທິເຂົ້າເຖິງ</a></li>
                        </ul>
                        {{-- ========= reports ============ --}}
                    <li class="dropdown-divider"></li>
                    <li class="nav-item dropdown">
                        <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false" class="nav-link dropdown-toggle"><i
                                class="fas fa-cog text-pink"></i>
                            ຕັ້ງຄ່າ</a>
                        <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
                            <li><a href="{{ route('backend.about') }}" class="dropdown-item"><i
                                        class="fa fa-angle-double-right text-pink"></i>
                                    ກ່ຽວກັບ</a></li>
                            <li class="dropdown-divider"></li>
                            <li><a href="{{ route('backend.slide') }}" class="dropdown-item"><i
                                        class="fa fa-angle-double-right text-pink"></i>
                                    ສະໄລຮູບພາບ</a></li>
                            <li class="dropdown-divider"></li>
                            <li><a href="{{ route('backend.post') }}" class="dropdown-item"><i
                                        class="fa fa-angle-double-right text-pink"></i>
                                    ໂພດສາທາລະນະ</a></li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>


        <!-- Right navbar links -->
        <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
            <!-- Notifications Dropdown Menu -->
            <div wire:poll wire:ignore>
                <a class="nav-link" data-toggle="dropdown" href="#">
                    <i class="far fa-bell"></i>
                    <span class="badge badge-success navbar-badge">{{ $preorder_count }}</span>
                </a>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                    <span class="dropdown-item dropdown-header">ມີ {{ $preorder_count }} ສັ່ງຈອງໃຫມ່</span>
            
                    <div class="dropdown-divider"></div>
                    @foreach ($preorder as $item)
                    <a href="{{ route('backend.preorder') }}" class="dropdown-item">
                        <span>{{ $item->code }}</span>
                        <i class="fas fa-user"></i>
                        @if(!empty($item->customer))
                        {{ $item->customer->name }}, ໂທ:{{ $item->customer->phone }}
                        <span class="float-right text-muted text-sm">{{ $item->updated_at->diffForHumans() }}</span>
                        @endif
                    </a>
                    @endforeach
                    <div class="dropdown-divider"></div>
                </div>
            
            </div>
            
                <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="#">
                        @if (!empty(Auth::guard('admin')->user()->image))
                            <img src="{{ Auth::guard('admin')->user()->image }}" style="width: 30px; height: 30px"
                                alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
                        @else
                            <img src="{{ asset('logo/noimage.png') }}" style="width: 30px; height: 30px"
                                alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
                        @endif
                        <span
                            class="brand-text font-weight-light text-md">{{ Auth::guard('admin')->user()->name }}</span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right p-0">
                        <a class="nav-link" data-toggle="nav-item"
                            href="{{ route('backend.profile', auth()->user()->id) }}">
                            <i class="fas  fa-user-tie"></i> ໂປຣຟາຍ
                        </a>
                        <div class="dropdown-divider"></div>
                        <a data-toggle="modal" data-target="#modal-default" class="nav-link"
                            data-controlsidebar-slide="true" role="button">
                            <i class="fas fa-sign-out-alt text-danger"></i> ອອກຈາກລະບົບ
                        </a>
                    </div>
                </li>

                <!-- Menu Footer-->
                <li class="user-footer">
                </li>
        </ul>
        </li>

        </ul>

        <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse"
            aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

    </div>
</nav>
{{-- =========================== modal logout ============================ --}}
<div class="modal fade" id="modal-default">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <p class="modal-title"> <i class="fas fa-sign-out-alt"></i> ອອກຈາກລະບົບ</p>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h5 class="text-center">ທ່ານຕ້ອງການອອກຈາກລະບົບບໍ່?</h5>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-danger" data-dismiss="modal">ຍົກເລີກ</button>
                <a type="button" href="{{ route('logout') }}" class="btn btn-primary">ຕົກລົງ</a>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
