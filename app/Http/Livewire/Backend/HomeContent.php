<?php

namespace App\Http\Livewire\Backend;

use Livewire\Component;

class HomeContent extends Component
{
    public function render()
    {
        return view('livewire.backend.home-content');
    }
}
