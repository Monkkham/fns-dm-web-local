<div>
    {{-- ======================================== name page ====================================================== --}}
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h6><i class="fas fa-users"></i> ຜູ້ໃຊ້ & ສິດທິ <i class="fa fa-angle-double-right"></i>
                        ຜູ້ໃຊ້ລະບົບ</h6>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">ຫນ້າຫຼັກ</a></li>
                        <li class="breadcrumb-item active">ຜູ້ໃຊ້ລະບົບ</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!--List users- table table-bordered table-striped -->
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="row">
                                        <div class="col-md-6">
                                            {{-- @foreach ($rolepermissions as $items)
                                            @if ($items->permissionname->name == 'action_expend') --}}
                                            {{-- <a wire:click="create" class="btn btn-primary btn-sm"
                                                href="javascript:void(0)"><i class="fa fa-plus-circle"></i>
                                                {{ __('lang.add') }}</a> --}}
                                            {{-- @endif
                                             @endforeach --}}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                    </div>
                                    <div class="input-group input-group-sm" style="width: 240px;">
                                        <input wire:model="search" type="text" class="form-control"
                                            placeholder="ຄົ້ນຫາ">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead class="bg-pink">
                                        <tr>
                                            @foreach ($rolepermissions as $items)
                                            @if ($items->permissionname->name == 'action_user')
                                            <th>ຈັດການ</th>
                                            @endif
                                            @endforeach
                                            <th>ລຳດັບ</th>
                                            <th>ລະຫັດ</th>
                                            <th>ຮູບພາບ</th>
                                            <th>ຊື່</th>
                                            <th>ນາມສະກຸນ</th>
                                            <th>ເພດ</th>
                                            <th>ເບີໂທ</th>
                                            <th>ສິດທິ</th>
                                            <th>ທີ່ຢູ່</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $num = 1;
                                        @endphp

                                        @foreach ($employee as $item)
                                            <tr>
                                                @foreach ($rolepermissions as $items)
                                                @if ($items->permissionname->name == 'action_user')
                                                <td>
                                                    <div class="btn-group">
                                                        <button wire:click="showEdit({{ $item->id }})" type="button"
                                                            class="btn bg-gradient-blue btn-sm"><i
                                                                class="fas fa-shield-alt"></i> ມອບສິດທິ</button>
                                                    </div>
                                                </td>
                                                @endif
                                                @endforeach

                                                <td>{{ $num++ }}</td>
                                                {{-- <td>
                                                    @if (!empty($item->image))
                                                    <a href="{{$item->image}}">
                                                        <img src="{{$item->image}}"
                                                            width="80px;" height="50px;">
                                                    </a>
                                                @else
                                                <p class="text-center">ບໍ່ມີໃບບິນ</p>
                                                @endif
                                                </td> --}}
                                                <td>{{ $item->code }}</td>
                                                <td>
                                                    @if (!empty($item->image))
                                                        <a href="{{ asset('employee') }}/{{ $item->image }}">
                                                            <img class="rounded"
                                                                src="{{ asset('employee') }}/{{ $item->image }}"
                                                                width="60px;" height="60px;">
                                                        </a>
                                                    @else
                                                        <img src="{{ asset('logo/noimage.png') }}" width="60px;"
                                                            height="60px;">
                                                    @endif
                                                </td>
                                                <td>{{ $item->name }}</td>
                                                <td>{{ $item->lastname }}</td>
                                                <td>
                                                    @if ($item->gender == 1)
                                                        <b class="text-success">ຍິງ</b>
                                                    @elseif($item->gender == 2)
                                                        <b class="text-info">ຊາຍ</b>
                                                    @else
                                                <td></td>
                                        @endif
                                        </td>
                                        <td>{{ $item->phone }}</td>
                                        @if (!empty($item->roles))
                                            @if($item->roles->name == "Admin")
                                            <td class="text-bold text-center">
                                                <p class="bg-danger rounded">{{ $item->roles->name }}</p>
                                            </td>
                                            @else
                                            <td class="text-bold text-center">
                                                <p class="bg-success rounded">{{ $item->roles->name }}</p>
                                            </td>
                                            @endif
                                        @else
                                            <td></td>
                                        @endif
                                        @if (!empty($item->province))
                                            <td>{{ $item->village->name_la }},{{ $item->district->name_la }},{{ $item->province->name_la }}
                                            </td>
                                        @else
                                            <td></td>
                                        @endif
                                        {{-- <td>{{ date('d-m-Y', strtotime($item->created_at)) }}</td> --}}
                                        </form>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <div class="float-right">
                                    {{ $employee->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{-- ========================== get roles =========================== --}}
<div wire:ignore.self class="modal fade" id="modal-roles">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-gradient-blue">
                <h4 class="modal-title"><i class="fas fa-handshake"></i> ມອບສິດຫນ້າທີ່ໃຫ້ {{ $name }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" wire:model="ID">
                <div class="col-sm-12">
                    <label class="text-center">ເລືອກສິດທິໃຫ້ກັບຜູ້ໃຊ້ລະບົບ</label>
                    <div class="col-md-12">
                        <div class="form-group">
                            <select wire:model="roles_id" class="form-control" >
                              <option value="" selected>ເລືອກສິດທິ</option>
                                @foreach($roles as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                            @error('roles_id')
                            <span style="color: red" class="error">{{ $message }}</span>
                        @enderror
                        </div>
                  </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-danger" data-dismiss="modal">ຍົກເລີກ</button>
                <button wire:click="update({{ $ID }})" type="button"
                    class="btn btn-success">ຕົກລົງ</button>
            </div>
        </div>
    </div>
</div>
</div>

@push('scripts')
    <script>
        window.addEventListener('show-modal-roles', event => {
            $('#modal-roles').modal('show');
        })
        window.addEventListener('hide-modal-roles', event => {
            $('#modal-roles').modal('hide');
        })
    </script>
@endpush
