<?php

namespace App\Http\Livewire\Backend\Service;

use Livewire\Component;
use App\Models\PreOrder;
use App\Models\PreOrderFood;
use Livewire\WithPagination;
use App\Models\PreOrderTable;

class ServiceContent extends Component
{
    public $code,$customer_id,$name,$total,$payment,$status,$created_at,$onepay_image,$search,$datetime,$qtyp,$add_more;
    public $ID,$food_data = [],$table_data = [];
    public $count_food_data,$sum_subtotal_food_data,$count_table_data,$customer_data;
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public function render()
    {
        $preorders = PreOrder::whereIn('status',[2,3,4,5,6])
        ->where('code','like','%' . $this->search. '%')
        ->paginate(5);
        return view('livewire.backend.service.service-content',compact('preorders'))->layout('layouts.backend.base');
    }
    public function confirm_success($ids)
    {
        // $ids = $this->ID;
        $data = PreOrder::find($ids);
        $data->status = 3;
        $data->save();
        $table_status = PreOrderTable::select('*')
        ->where('preorder_id', $ids)
        ->join('tables', 'preorder_table.tables_id', '=', 'tables.id')->update(array('status' => 0));
        $this->dispatchBrowserEvent('hide-onepay');
        // $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບຂໍ້ມູນສຳເລັດ!']);
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ຊຳລະເງີນສຳເລັດເເລ້ວ!',
            'icon'=>'success',
            'iconColor'=>'green',
        ]);
    }
    public function confirm_checkbill($ids)
    {
        // $ids = $this->ID;
        $data = PreOrder::find($ids);
        $data->status = 5;
        $data->save();
        $table_status = PreOrderTable::select('*')
        ->where('preorder_id', $ids)
        ->join('tables', 'preorder_table.tables_id', '=', 'tables.id')->update(array('status' => 0));
        $this->dispatchBrowserEvent('hide-onepay');
        // $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບຂໍ້ມູນສຳເລັດ!']);
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ສົ່ງໃບບິນໃຫ້ຜູ້ຈັດການ!',
            'icon'=>'success',
            'iconColor'=>'green',
        ]);
    }
    public function confirm_service($ids)
    {
        // $ids = $this->ID;
        $data = PreOrder::find($ids);
        $data->status = 4;
        $data->save();
        $this->dispatchBrowserEvent('hide-onepay');
        // $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບຂໍ້ມູນສຳເລັດ!']);
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ຮ້ອງພະນັກງານເສີບ!',
            'icon'=>'success',
            'iconColor'=>'green',
        ]);
    }
    public function confirm_cooking($ids)
    {
        // $ids = $this->ID;
        $data = PreOrder::find($ids);
        $data->status = 3;
        $data->save();
        $this->dispatchBrowserEvent('hide-onepay');
        // $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບຂໍ້ມູນສຳເລັດ!']);
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ຮັບຄົວກິນ!',
            'icon'=>'success',
            'iconColor'=>'green',
        ]);
    }
    public function Bill_Customer($ids)
    {
        $this->dispatchBrowserEvent('show-modal-bill-customer');
        $data = PreOrder::find($ids);
        $this->ID = $data->id;
        $this->customer_data = $data->customer;
        $this->code = $data->code;
        $this->qtyp = $data->qtyp;
        $this->payment = $data->payment;
        $this->status = $data->status;
        $this->add_more = $data->add_more;
        $this->datetime = $data->datetime;

        $this->food_data = PreOrderFood::where('preorder_id', $ids)->get();
        $this->count_food_data = PreOrderFood::where('preorder_id', $ids)->count();
        $this->sum_subtotal_food_data = PreOrderFood::where('preorder_id', $ids)->sum('subtotal');

        $this->table_data = PreOrderTable::where('preorder_id', $ids)->get();
        $this->count_table_data = PreOrderTable::where('preorder_id', $ids)->count();
    }
    public function confirm_add_more($ids)
    {
        // $data = PreOrder::find($ids);
        // $data->save();
        $food_add_more = PreOrderFood::where('preorder_id', $ids)
        ->update(array('add_more' => 0));
        $this->dispatchBrowserEvent('hide-modal-bill-customer');
        // $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບຂໍ້ມູນສຳເລັດ!']);
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ຍືນຍັນຮັບເມນູເພີ່ມເຕີມ!',
            'icon'=>'success',
            'iconColor'=>'green',
        ]);
    }
    public function ShowBill($ids)
    {
        $this->dispatchBrowserEvent('show-modal-bill');
        $data = PreOrder::find($ids);
        $this->ID = $data->id;
        $this->customer_data = $data->customer;
        $this->code = $data->code;
        $this->qtyp = $data->qtyp;
        $this->payment = $data->payment;
        $this->status = $data->status;
        $this->datetime = $data->datetime;

        $this->food_data = PreOrderFood::where('preorder_id', $ids)->get();
        $this->count_food_data = PreOrderFood::where('preorder_id', $ids)->count();
        $this->sum_subtotal_food_data = PreOrderFood::where('preorder_id', $ids)->sum('subtotal');

        $this->table_data = PreOrderTable::where('preorder_id', $ids)->get();
        $this->count_table_data = PreOrderTable::where('preorder_id', $ids)->count();
    }
}
