<?php

namespace App\Http\Livewire\Backend\Store;

use Livewire\Component;
use App\Models\Abouts;
use Livewire\WithFileUploads;

class AboutContent extends Component
{
    use WithFileUploads;
    public $name, $address,$phone,$email,$note,$role,$lats,$longs,$latitude,$longitude;
    public $new_img, $img;
    public function mount()
    {
        $data = Abouts::find(1);
        $this->name = $data->name;
        $this->address = $data->address;
        $this->phone = $data->phone;
        $this->email = $data->email;
        $this->note = $data->note;
        $this->role = $data->role;
        $this->latitude = $data->latitude;
        $this->longitude = $data->longitude;
        // $this->new_img = $data->logo;
    }
    public function render()
    {
        return view('livewire.backend.store.about-content')->layout('layouts.backend.base');
    }
    public function update()
    {
        $this->validate([
            'name'=>'required',
            'phone'=>'required|regex:/^[0-9]+$/i|max:8||unique:customer',
            'note'=>'required',
            'address'=>'required',
        ],[
            'name.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            'phone.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            'phone.regex'=>'ປ້ອນເປັນຕົວເລກທັງຫມົດ!',
            'phone.max'=>'ຕົວເລກບໍ່ເກີນ 8 ໂຕ!',
            'phone.unique'=>'ຂໍ້ມູນນີ້ມີໃນລະບົບເເລ້ວ!',
            'note.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            // 'image.required'=>'ເລືອກຮູບພາບກ່ອນ!',
            'address.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
        ]);
        $data = Abouts::find(1);
        // if (!empty($this->img)) {
        //     $this->validate([
        //         'img' => 'required|mimes:jpg,png,jpeg',
        //     ]);
        //     $imgName = date('ymdhis').'_'.$this->img->getClientOriginalName(). '.' . $this->img->extension();
        //     $this->img->storeAs('upload/aboute/', $imgName);
        //     $data->logo = 'upload/aboute/'.$imgName;
        // }
        $data->name = $this->name;
        $data->address = $this->address;
        $data->phone = $this->phone;
        $data->email = $this->email;
        $data->note = $this->note;
        $data->role = $this->role;
        $data->latitude = $this->lats;
        $data->longitude = $this->longs;
        $data->save();
        session()->flash('success', 'ແກ້ໄຂຂໍ້ມູນສຳເລັດ');
        return redirect(route('backend.about'));
    }
}
