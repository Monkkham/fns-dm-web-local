<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a href="index3.html" class="nav-link"><i class="fas fa-globe"></i> ໄປທີ່ເວບໄຊ</a>
        </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        <li class="nav-item">
            <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                <i class="fas fa-expand-arrows-alt"></i>
            </a>
        </li>
        @auth
        @if (Auth::guard('admin')->user()->id != 1)
        <li class="nav-item dropdown">
            <a class="nav-link" href="{{ route('backend.profile', auth()->user()->id) }}">
                @if (!empty(Auth::guard('admin')->user()->image))
                <img src="{{ asset('profile') }}/{{ Auth::guard('admin')->user()->image }}"
                    style="width: 30px; height: 30px" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
                    style="opacity: .8">
                @else
                <img src="{{ asset('logo/noimage.png') }}" style="width: 30px; height: 30px" alt="AdminLTE Logo"
                    class="brand-image img-circle elevation-3" style="opacity: .8">
                @endif
                <span class="brand-text font-weight-light text-md">{{ Auth::guard('admin')->user()->name }}</span>
            </a>
        </li>
        @endif
        @endauth
        {{-- ============================= modal logout ============================= --}}
        <li class="nav-item">
            <a data-toggle="modal" data-target="#modal-default" class="nav-link" data-controlsidebar-slide="true"
                role="button">
                <i class="fas fa-sign-out-alt text-danger"></i>
            </a>
        </li>
    </ul>
</nav>
{{-- =========================== logout ============================ --}}
<div class="modal fade" id="modal-default">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <p class="modal-title"> <i class="fas fa-sign-out-alt"></i> {{ __('lang.logout') }}</p>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h5 class="text-center">{{ __('lang.do_you_want_to_logout') }}</h5>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-danger" data-dismiss="modal">{{ __('lang.cancle') }}</button>
                <a type="button" href="{{ route('logout') }}" class="btn btn-primary">{{ __('lang.ok') }}</a>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.navbar -->
