<?php

namespace App\Models;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Employee extends Authenticatable 
{
    use HasApiTokens, HasFactory, Notifiable;
    protected $table = "employee";
    protected $fillable = ['id','roles_id','village_id','district_id','province_id','code','name','lastname','phone','password','image','gender','status','address','email','birthday','status','created_at','updated_at'];
    public function roles()
    {
        return $this->belongsTo('App\Models\Roles','roles_id','id');
    }
    public function village()
    {
        return $this->belongsTo('App\Models\Villages','village_id','id');
    }
    public function district()
    {
        return $this->belongsTo('App\Models\Districts','district_id','id');
    }
    public function province()
    {
        return $this->belongsTo('App\Models\Provinces','province_id','id');
    }
}
