<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostPublic extends Model
{
    use HasFactory;
    protected $table = "post_public";
    protected $fillable = ['id','note','image','created_at','updated_at'];
}
