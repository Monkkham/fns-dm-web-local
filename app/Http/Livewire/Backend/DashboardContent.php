<?php

namespace App\Http\Livewire\Backend;

use App\Models\Foods;
use App\Models\Orders;
use Livewire\Component;
use App\Models\Customer;
use App\Models\Employee;
use App\Models\NoteBook;
use App\Models\PreOrder;
use App\Models\Products;
use App\Models\Supplier;

class DashboardContent extends Component
{
    public $search, $count_employee,$count_customer,$count_foods,$count_supplier,$count_product,$count_preorder;
    public $sum_income,$sum_expend,$sum_expend2,$sum_expend3;
    public function render()
    {
        $this->count_employee = Employee::count();
        $this->count_customer = Customer::count();
        $this->count_supplier = Supplier::count();
        $this->count_foods = Foods::count();
        $this->count_product = Products::count();
        $this->count_preorder = PreOrder::count();
        $preorders = PreOrder::where('status',1)
        ->where('code','like','%' . $this->search. '%')
        ->paginate(5);
        $this->sum_income = PreOrder::where('status',3)->sum('total');
        $this->sum_expend = Orders::where('status',2)->sum('total');
        $this->sum_expend2 = NoteBook::where('type',2)->sum('money');
        $this->sum_expend3 = NoteBook::where('type',1)->sum('money');
        return view('livewire.backend.dashboard-content',compact('preorders'))->layout('layouts.backend.base');
    }
}
