<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    use HasFactory;
    protected $table = "supplier";
    protected $fillable = ['id','village_id','district_id','province_id','image','name','lastname','phone','email','gender','address','created_at','updated_at'];
    public function village()
    {
        return $this->belongsTo('App\Models\Villages','village_id','id');
    }
    public function district()
    {
        return $this->belongsTo('App\Models\Districts','district_id','id');
    }
    public function province()
    {
        return $this->belongsTo('App\Models\Provinces','province_id','id');
    }
}
