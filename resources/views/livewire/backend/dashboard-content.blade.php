<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h5><i class="nav-icon fas fa-tachometer-alt"></i> ຫນ້າຫຼັກຜູ້ດູເເລລະບົບ</h5>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">ຫນ້າຫຼັກ</a></li>
                    <li class="breadcrumb-item active">ຫນ້າຫຼັກ</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3 col-sm-6 col-12">
                <div class="info-box">
                    <span class="info-box-icon bg-info"><i class="fas fa-user-tie"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">ພະນັກງານ</span>
                        <span class="info-box-number">ທັງຫມົດ: {{ $this->count_employee }} ຄົນ</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-12">
                <div class="info-box">
                    <span class="info-box-icon bg-success"><i class="fas fa-users"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">ລູກຄ້າ</span>
                        <span class="info-box-number">ທັງຫມົດ: {{ $this->count_customer }} ຄົນ</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-12">
                <div class="info-box">
                    <span class="info-box-icon bg-warning"><i class="fas fa-user-tie"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">ຜູ້ສະຫນອງ</span>
                        <span class="info-box-number">ທັງຫມົດ: {{ $this->count_supplier }} ຄົນ</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-12">
                <div class="info-box">
                    <span class="info-box-icon bg-danger"><i class="fas fa-utensils"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">ເມນູອາຫານ</span>
                        <span class="info-box-number">ທັງຫມົດ: {{ $this->count_foods }} ລາຍການ</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- =========================================================== -->

        <!-- Small Box (Stat card) -->
        <div class="row">
            <div class="col-lg-3 col-6">
                <!-- small card -->
                <div class="small-box bg-info">
                    <div class="inner">
                        <h4>{{ $this->count_product }} ລາຍການ</h4>
                        <p>ຈຳນວນສິນຄ້າ</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-box-open"></i>
                    </div>
                    <a href="#" class="small-box-footer">
                        ລາຍລະອຽດ <i class="fas fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
                <!-- small card -->
                <div class="small-box bg-success">
                    <div class="inner">
                        <h4>{{ $this->count_preorder }} ລາຍການ</h4>

                        <p>ລາຍການສັ່ງຈອງ</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-users"></i>
                    </div>
                    <a href="#" class="small-box-footer">
                        ລາຍລະອຽດ <i class="fas fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
                <!-- small card -->
                <div class="small-box bg-warning">
                    <div class="inner">
                        <h4>ລວມຍອດລາຍຮັບ</h4>

                        <small>ຈາກການຂາຍ: {{ number_format($this->sum_income) }} ກີບ <br>
                        ປະຈຳວັນ: {{ number_format($this->sum_expend3) }} ກີບ</small>

                    </div>
                    <div class="icon">
                        <i class="fas fa-money-check-alt"></i>
                    </div>
                    <a href="#" class="small-box-footer">
                        ລາຍລະອຽດ <i class="fas fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
                <!-- small card -->
                <div class="small-box bg-danger">
                    <div class="inner">
                        <h4>ລວມຍອດລາຍຈ່າຍ</h4>

                        <small>
                            ຈາກການຊື້: {{ number_format($this->sum_expend) }} ກີບ <br>
                            ປະຈຳວັນ: {{ number_format($this->sum_expend2) }} ກີບ
                        </small>
                    </div>
                    <div class="icon">
                        <i class="fas fa-hand-holding-usd"></i>
                    </div>
                    <a href="#" class="small-box-footer">
                        ລາຍລະອຽດ <i class="fas fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
            <!-- ./col -->
        </div>
        <!-- /.row -->
        {{-- ========================================== --}}
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead class="bg-info">
                            <tr>
                                <th class="bg-light text-center" colspan="10"><i class="fas fa-users"></i>
                                    ລາຍການສັ່ງຈອງໃຫມ່ລ່າສຸດ</th>
                            </tr>
                            <tr class="text-center bg-pink">
                                <th>ລຳດັບ</th>
                                <th>ລະຫັດບິນ</th>
                                <th>ເງິນມັດຈຳ</th>
                                <th>ລູກຄ້າ</th>
                                <th>ເພດ</th>
                                <th>ເບີໂທ</th>
                                <th>ທຸລະກຳ</th>
                                <th>ວັນທີ່ຈອງ</th>
                                <th>ສະຖານະ</th>
                                {{-- @foreach ($rolepermissions as $items)
                            @if ($items->permissionname->name == 'action_employee') --}}
                                <th>ຈັດການ</th>
                                {{-- @endif
                            @endforeach --}}
                            </tr>
                        </thead>
                        @php
                            $num = 1;
                        @endphp
                        <tbody>

                            @foreach ($preorders as $item)
                                <tr class="text-center">
                                    <td>{{ $num++ }}</td>
                                    <td>{{ $item->code }}</td>
                                    <td class="text-bold">{{ number_format($item->total) }} ₭</td>
                                    <td>
                                        @if (!empty($item->customer))
                                            {{ $item->customer->name }} {{ $item->customer->lastname }}
                                        @endif
                                    </td>
                                    <td>
                                        @if (!empty($item->customer))
                                            @if ($item->customer->gender == 1)
                                                <b class="text-success">ຍິງ</b>
                                            @elseif($item->customer->gender == 2)
                                                <b class="text-info">ຊາຍ</b>
                                            @endif
                                        @endif
                                    </td>
                                    <td>
                                        @if (!empty($item->customer))
                                            {{ $item->customer->phone }}
                                        @endif
                                    </td>
                                    <td>
                                        @if ($item->payment == 1)
                                            <button type="button" class="btn btn-sm btn-outline-danger"><i
                                                    class="fa fa-credit-card"></i>
                                                OnePay</button>
                                        @elseif($item->payment == 0)
                                            <p class="badge badge-success p-2"><i class="fa fa-check-circle"></i>
                                                ຊຳລະເເລ້ວ</p>
                                        @endif
                                    </td>
                                    <td>{{ date('d/m/Y', strtotime($item->updated_at)) }}</td>
                                    <td>
                                        @if ($item->status == 1)
                                            <p class="bg-success text-center rounded"><i class="fas fa-plus-circle"></i>
                                                ໃຫມ່ <span
                                                    class="spinner-grow mb-1 spinner-grow-sm text-white text-center"
                                                    role="status" aria-hidden="true"></span></p>
                                        @elseif($item->status == 2)
                                            <p class="bg-success text-center text-white rounded"><i
                                                    class="fa fa-check-circle"></i> ນຳເຂົ້າສຳເລັດ</p>
                                        @elseif($item->status == 0)
                                            <p class="bg-danger text-center text-white rounded"><i
                                                    class="fas fa-times-circle"></i> ຖືກຍົກເລີກ</p>
                                        @endif
                                    </td>
                                    {{-- <td>{{ date('d-m-Y', strtotime($item->created_at)) }}</td> --}}
                                    {{-- @foreach ($rolepermissions as $items)
                                @if ($items->permissionname->name == 'action_preorders') --}}
                                    <td>
                                        {{-- @if ($item->payment != 1 && $item->status != 0) --}}
                                        <a href="{{ route('backend.preorder') }}" class="btn btn-primary btn-sm">
                                            ໄປທີ່ລາຍການ <i class="fas fa-arrow-alt-circle-right"></i>
                                        </a>
                                        {{-- @endif --}}
                                    </td>
                                    {{-- @endif
                                @endforeach --}}
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="float-right">
                        {{ $preorders->links() }}
                    </div>

                    <div class="d-flex justify-content-between">
                        <div>
                            {{-- {{$sale->links()}} --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->
