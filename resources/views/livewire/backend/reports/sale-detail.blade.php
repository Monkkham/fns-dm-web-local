<!-- Main content -->
<div wire:ignore.self class="modal fade" id="modal-detail">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-light">
                <h5 class="modal-title"><i class="fas fa-users"></i> <b>ລາຍລະອຽດລາຍງານການຂາຍ</b></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="invoice p-3 mb-3">
                    <!-- title row -->
                    <div class="row">
                         <!-- info row -->
                    <div class="invoice-info">
                        <div class="col-sm-12 invoice-col">
                            <address>
                                <strong><i class="fas fa-user-tie"></i> ລູກຄ້າ</strong><br>
                                @if (!empty($this->customer_data))
                                    <b>ຊື່:</b> {{ $this->customer_data->name }} {{ $this->customer_data->lastname }}
                                    <br>
                                    <b>ຈຳນວນຄົນ</b> {{ $this->qtyp }} ທ່ານ <br>
                                    <b>ເບີໂທ:</b> {{ $this->customer_data->phone }} <br>
                                    <b>ອີເມວ:</b> {{ $this->customer_data->email }} <br>
                                    <b>ທີ່ຢູ່:</b> 
                                    @if(!empty($this->customer_data->village))
                                    {{ $this->customer_data->village->name_la }},
                                    {{ $this->customer_data->district->name_la }},
                                    {{ $this->customer_data->province->name_la }} <br>
                                    @endif
                                   
                                @endif
                            </address>
                        </div>
                    </div>
                    <!-- /.row -->
                    <div class="invoice-info" style="padding-left: 280px">
                        <div class="col-sm-12 invoice-col">
                            <h4>
                                <small class="float-right text-sm"><b><i class="fas fa-calendar-alt"></i> ວັນທີ່ນັດຫມາຍ:</b>
                                    {{ date('d-m-Y', strtotime($this->datetime)) }} <br> <b><i class="fas fa-clock"></i> ເວລາ:</b>
                                    {{ date('H:i:s', strtotime($this->datetime)) }}</small>
                            </h4>
                        </div>
                    </div>
                        <!-- /.col -->
                    </div>
                   
   <!-- Table row -->
   <div class="row">
    <div class="col-12 table-responsive">
        <table class="table table-striped">
            <thead>
                <tr>
                    <td colspan="4" class="text-center"><h5 class="text-bold"><i class="fas fa-chair"></i> ລາຍການໂຕະທີ່ຈອງ</h5></td>
                </tr>
                <tr class="bg-pink">
                    <th>ລຳດັບ</th>
                    <th>ໂຊນຮ້ານ</th>
                    <th>ລະຫັດໂຕະ</th>
                    <th>ຈຳນວນຕັ່ງ</th>
                </tr>
            </thead>
            @php
                $num = 1;
            @endphp
            <tbody>
                @foreach ($this->table_data as $item)
                    @if (!empty($item->tables))
                        <tr>
                            <td>{{ $num++ }}</td>
                            <td class="text-bold">{{ $item->tables->zones->name }}</td>
                            <td>{{ $item->tables->code }}</td>
                            <td>{{ $item->tables->chiar_qty }} ຫນ່ວຍ</td>
                        </tr>
                    @endif
                @endforeach
            </tbody>
        </table>
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->
                    <!-- Table row -->
                    <div class="row">
                        <div class="col-12 table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <td colspan="6" class="text-center"><h5 class="text-bold"><i class="fas fa-utensils"></i> ລາຍການອາຫານທີ່ຈອງ</h5></td>
                                    </tr>
                                    <tr class="bg-pink">
                                        <th>ລຳດັບ</th>
                                        <th>ເມນູ</th>
                                        <th>ປະເພດ</th>
                                        <th>ລາຄາ</th>
                                        <th>ຈຳນວນ</th>
                                        <th>ເປັນເງິນ</th>
                                    </tr>
                                </thead>
                                @php
                                    $num = 1;
                                @endphp
                                <tbody>
                                    @foreach ($this->food_data as $item)
                                        @if (!empty($item->foods))
                                            <tr>
                                                <td>{{ $num++ }}</td>
                                                <td class="text-bold">{{ $item->foods->name }}</td>
                                                <td>{{ $item->foods->foods_type->name }}</td>
                                                <td>{{ number_format($item->foods->price) }} ₭</td>
                                                <td>{{ $item->food_qty }}</td>
                                                <td>{{ number_format($item->foods->price * $item->food_qty) }} ₭</td>
                                            </tr>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                    <div class="row">
                        <div class="col-5">
                            <div class="table-responsive">
                                <table class="table">
                                    <tr>
                                        <th style="width:50%">ລວມໂຕະ:</th>
                                        <td class="text-sm">{{ $this->count_table_data }} ໂຕະ
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="width:50%">ລວມອາຫານ:</th>
                                        <td class="text-sm">{{ $this->count_food_data }} ລາຍການ
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>ລວມເປັນເງິນ:</th>
                                        <td class="text-sm text-bold">{{ number_format($this->sum_subtotal_food_data) }} ₭
                                    </tr>
                                    <tr>
                                        <th>ທຸລະກຳ:</th>
                                        @if ($this->payment == 1)
                                            <td class="text-danger"><i class="fas fa-hand-holding-usd"></i> ບໍ່ທັນຊຳລະ</td>
                                        @elseif($this->payment == 2)
                                            <td class="text-success"><i class="fas fa-check-circle"></i> ຊຳລະເເລ້ວ</td>
                                        @endif
                                    </tr>
                                    <tr>
                                        <th>ສະຖານະ:</th>
                                        @if ($this->status == 1)
                                            <td class="text-success"><i class="fas fa-plus-circle"></i> ໃຫມ່</td>
                                        @elseif($this->status == 2)
                                            <td class="text-info"><i class="fas fa-check-circle"></i> ອານຸມັດເເລ້ວ</td>
                                            @elseif($this->status == 4)
                                            <td class="text-warning"><i class="fas fa-users"></i> ກຳລັງບໍລິການ</td>
                                            @elseif($this->status == 3)
                                            <td class="text-success"><i class="fas fa-check-circle"></i> ບໍລິການສຳເລັດ</td>
                                            @elseif($this->status == 0)
                                            <td class="text-danger"><i class="fas fa-times-circle"></i> ຖືກຍົກເລີກ</td>
                                        @endif
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.invoice -->
