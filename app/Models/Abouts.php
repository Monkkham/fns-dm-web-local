<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Abouts extends Model
{
    use HasFactory;
    protected $table = "abouts";
    protected $fillable = [
        'id',
        'name',
        'address',
        'phone',
        'email',
        'note',
        'role',
        'latitude',
        'longitude',
        'created_at',
        'updated_at'
    ];
}
