<?php

namespace App\Http\Livewire\Backend;
use DB;
use Livewire\Component;
use App\Models\Employee;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
class ProfileContent extends Component
{
    use WithFileUploads;
    public 
    $customer_type_id,
    $roles_id,
    $village_id,
    $district_id,
    $province_id,
    $code,
    $image,
    $name,
    $lastname,
    $password,
    $phone,
    $email,
    $address,
    $gender,
    $created_at,
    $updated_at;
    public $old_password, $confirmpassword;
    public $villages = [];
    public $districts = [];
    public function mount()
    {
        $user = Employee::where('id', auth()->user()->id)->first();
        $this->roles_id = $user->roles_id;
        $this->village_id = $user->village_id;
        $this->district_id = $user->district_id;
        $this->province_id = $user->province_id;
        $this->code = $user->code;
        $this->image = $user->image;
        $this->name = $user->name;
        $this->lastname = $user->lastname;
        $this->phone = $user->phone;
        $this->email = $user->email;
        $this->address = $user->address;
        $this->gender = $user->gender;
    }
    public function render()
    {
        return view('livewire.backend.profile-content')->layout('layouts.backend.base');
    }
    public function resetform()
    {
        $this->image = "";
    }
    public function updateProfile()
    {
        // if($updateID > 0)
        // {
        //     $this->validate([
        //         'code'=>'required',
        //         'name'=>'required',
        //         'lastname'=>'required',
        //         'phone'=>'required',
        //     ],[
        //         'code.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
        //         'name.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
        //         'lastname.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
        //         'phone.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
        //     ]);
        // }
        if(!empty($this->password))
        {
           if(!empty($this->old_password)){
           $data = Employee::find(auth()->user()->id);
           $var = password_verify($this->old_password, $data->password);
           if($var){
            $data->name = $this->name;
            $data->lastname = $this->lastname;
            $data->phone = $this->phone;
            $data->email = $this->email;
            $data->address = $this->address;
            $data->gender = $this->gender;
            $data->email = $this->email;
            if ($this->image) {
                $this->validate([
                    'image' => 'required|mimes:png,jpg,jpeg',
                ]);
                if ($this->image) {
                    $this->validate([
                        'image' => 'required|mimes:png,jpg,jpeg',
                    ]);
                    if ($this->image) {
                        $this->validate([
                            'image' => 'required|mimes:png,jpg,jpeg',
                        ]);
                        if ($this->image != $data->image) {
                            if (!empty($data->image)) {
                                $images = explode(",", $data->images);
                                foreach ($images as $image) {
                                    unlink('' . '' . $data->image);
                                }
                                $data->delete();
                            }
                        }
                        $imageName = Carbon::now()->timestamp . '.' . $this->image->extension();
                        $this->image->storeAs('upload/employee', $imageName);
                        $data->image = 'upload/employee'.'/'.$imageName;
                    }
                }
            }
            $data->save();
            $this->resetform();
            $this->emit('alert', ['type' => 'success', 'message' => 'ແກ້ໄຂໂປຣຟາຍສຳເລັດເເລ້ວ!']);
            return redirect(route('backend.profile',auth()->user()->id));
           }else{
            $this->dispatchBrowserEvent('swal', [
                'title' => 'ລະຫັດຜ່ານບໍ່ຖືກຕ້ອງ ກະລຸນາກວດຄືນ!',
                'icon'=>'error',
                'iconColor'=>'red',
            ]);
           }
        }else{
            $this->validate([
                'old_password'=>'required',
            ],[
                'old_password.required'=>'ກະລຸນາປ້ອນລະຫັດຜ່ານເກົ່າກ່ອນ',
            ]);
        }
        }else{
            if(!empty($this->old_password)){
                $data = Employee::find(auth()->user()->id);
                $var = password_verify($this->old_password, $data->password);
                if($var){
                    $data->name = $this->name;
                    $data->lastname = $this->lastname;
                    $data->phone = $this->phone;
                    $data->email = $this->email;
                    $data->address = $this->address;
                    $data->gender = $this->gender;
                    $data->email = $this->email;
                    if ($this->image) {
                        $this->validate([
                            'image' => 'required|mimes:png,jpg,jpeg',
                        ]);
                        if ($this->image) {
                            $this->validate([
                                'image' => 'required|mimes:png,jpg,jpeg',
                            ]);
                            if ($this->image != $data->image) {
                                if (!empty($data->image)) {
                                    $images = explode(",", $data->images);
                                    foreach ($images as $image) {
                                        unlink('' . '' . $data->image);
                                    }
                                    $data->delete();
                                }
                            }
                            $imageName = Carbon::now()->timestamp . '.' . $this->image->extension();
                            $this->image->storeAs('upload/employee', $imageName);
                            $data->image = 'upload/employee'.'/'.$imageName;
                        }
                    }
                    $data->save();
                    $this->emit('alert', ['type' => 'success', 'message' => 'ແກ້ໄຂໂປຣຟາຍສຳເລັດເເລ້ວ!']);
                    return redirect(route('backend.profile',auth()->user()->id));
                }else{
                    $this->dispatchBrowserEvent('swal', [
                        'title' => 'ລະຫັດຜ່ານບໍ່ຖືກຕ້ອງ ກະລຸນາກວດຄືນ!',
                        'icon'=>'error',
                        'iconColor'=>'red',
                    ]);
                }
             }else{
                 $this->validate([
                     'old_password'=>'required',
                 ],[
                     'old_password.required'=>'ປ້ອນລະຫັດຜ່ານເກົ່າກ່ອນ!',
                 ]);
             }
        }
    }
}
