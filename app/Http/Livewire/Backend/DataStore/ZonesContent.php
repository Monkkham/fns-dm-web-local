<?php

namespace App\Http\Livewire\Backend\DataStore;

use App\Models\Zones;
use Livewire\Component;
use Livewire\WithPagination;

class ZonesContent extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $ID, $name, $code, $search;
    public function render()
    {
        $zones = Zones::orderBy('id', 'desc')
            ->where('name', 'like', '%' . $this->search . '%')
            ->paginate(5);
        return view('livewire.backend.data-store.zones-content', compact('zones'))->layout('layouts.backend.base');
    }
    public function resetform()
    {
        $this->name = '';
        $this->ID = '';
    }
    protected $rules = [
        'name' => 'required|unique:zones',
    ];
    protected $messages = [
        'name.required' => 'ກະລຸນາປ້ອນຂໍ້ມູນກ່ອນ!',
        'name.unique' => 'ຂໍ້ມູນນີ້ມີໃນລະບົບເເລ້ວ!',
    ];
    // public function updated($propertyName)
    // {
    //     $this->validateOnly($propertyName);
    // }
    public function store()
    {
        $updateId = $this->ID;
        if ($updateId > 0) {
            $this->validate([
                'name' => 'required',
            ], [
                'name.required' => 'ກະລຸນາປ້ອນຂໍ້ມູນກ່ອນ!',
            ]);
            $data = Zones::find($updateId);
            $data->update([
                'name' => $this->name,
            ]);
            $this->dispatchBrowserEvent('swal', [
                'title' => 'ແກ້ໄຂຂໍ້ມູນສຳເລັດ !',
                'icon' => 'success',
                'iconColor' => 'green',
            ]);
            $this->resetform();
        } else //ເພີ່ມໃໝ່
        {
            $this->validate([
                'name' => 'required|unique:zones',
            ], [
                'name.required' => 'ກະລຸນາປ້ອນຂໍ້ມູນກ່ອນ!',
                'name.unique' => 'ຂໍ້ມູນນີ້ມີໃນລະບົບເເລ້ວ!',
            ]);
            $employee_max = Zones::count('id');
            $count = $employee_max + 1;
            $data = new Zones();
            if (!empty($employee_max)) {
                $data->code = 'ZN-00' . $count;
            } else {
                $data->code = 'ZN-001';
            }
            $data->name = $this->name;
            $data->save();
            $this->dispatchBrowserEvent('swal', [
                'title' => 'ເພີ່ມຂໍ້ມູນສຳເລັດ !',
                'icon' => 'success',
                'iconColor' => 'green',
            ]);
            $this->resetform();
        }
        // $this->emit('alert', ['type' => 'success', 'message' => 'ເພີ່ມຂໍ້ມູນສຳເລັດ!']);
    }
    public function edit($ids)
    {
        $data = Zones::find($ids);
        $this->name = $data->name;
        $this->ID = $data->id;
    }
    public function showDestroy($ids)
    {
        $this->dispatchBrowserEvent('show-modal-delete');
        $data = Zones::find($ids);
        $this->ID = $data->id;
        $this->name = $data->name;
    }

    public function destroy($ids)
    {
        $ids = $this->ID;
        $data = Zones::find($ids);
        $data->delete();
        $this->dispatchBrowserEvent('hide-modal-delete');
        // $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບຂໍ້ມູນສຳເລັດ!']);
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ລຶບຂໍ້ມູນສຳເລັດ !',
            'icon' => 'success',
            'iconColor' => 'green',
        ]);
        $this->resetform();
    }
    // public function destroy($ids)
    // {
    //     $ids = $this->ID;
    //     $product_type = Zones::find($ids);
    //         $product_type->del = 0;
    //         $product_type->save();
    //         $this->dispatchBrowserEvent('hide-modal-delete');
    //         $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບຂໍ້ມູນສຳເລັດ!']);
    //         $this->dispatchBrowserEvent('swal', [
    //             'title' => 'ລຶບຂໍ້ມູນສຳເລັດ !',
    //             'icon'=>'success',
    //             'iconColor'=>'green',
    //         ]);
    //         $this->resetdata();
    // }
}
