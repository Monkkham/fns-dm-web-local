    <!-- /.modal-add -->
    <div wire:ignore.self class="modal fade" id="modal-add">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header bg-light">
                    <h5 class="modal-title"><i class="fa fa-plus text-success"></i> ເພີ່ມໃຫມ່</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="container">
                            <div wire:ignore class="avatar-upload">
                                <div class="avatar-edit">
                                    <input type='file' wire:model="image" id="imageUpload"
                                        accept=".png, .jpg, .jpeg" />
                                    <label for="imageUpload"></label>
                                </div>
                                <label class="text-center">ໃສ່ຮູບພາບ(ຖ້າມີ)</label>
                                <div class="avatar-preview">
                                    <div id="imagePreview"
                                        style="background-image: url({{ asset('logo/noimage.jpg') }});">
                                    </div>
                                </div>
                            </div>
                        </div>
              
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>ເລືອກປະເພດສິນຄ້າ</label>
                                    <select wire:model="product_type_id" class="form-control">
                                        <option value="" selected>ເລືອກ</option>
                                        @foreach ($product_type as $item)
                                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('product_type_id')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>ເລືອກຫົວຫນ່ວຍ</label>
                                    <select wire:model="unit_id" class="form-control">
                                        <option value="" selected>ເລືອກ</option>
                                        @foreach ($units as $item)
                                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('unit_id')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            {{-- <div class="col-md-3">
                                <div class="form-group">
                                    <label>ເລືອກຫົວຫນ່ວຍ</label>
                                    <select wire:model="unit_id" class="form-control">
                                        <option value="" selected>ເລືອກ</option>
                                        @foreach ($units as $item)
                                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('unit_id')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div> --}}
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>ຊື່</label>
                                    <input wire:model="name" type="text" placeholder="ປ້ອນຂໍ້ມູນ"
                                        class="form-control @error('name') is-invalid @enderror">
                                    @error('name')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>ລາຄານຳເຂົ້າ</label>
                                    <input wire:model="buy_price" type="number" min="1" placeholder="ປ້ອນຂໍ້ມູນ"
                                        class="form-control @error('buy_price') is-invalid @enderror">
                                    @error('buy_price')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            {{-- <div class="col-md-3">
                                <div class="form-group">
                                    <label>ລາຄາໂປຣໂມຊັ່ນ</label>
                                    <input wire:model="promotion_price" type="text" placeholder="ປ້ອນຂໍ້ມູນ"
                                        class="form-control @error('promotion_price') is-invalid @enderror">
                                    @error('promotion_price')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div> --}}
                            {{-- <div class="col-md-3">
                                <div class="form-group">
                                    <label>ສະຕ໋ອກ</label>
                                    <input wire:model="stock" type="text" placeholder="ປ້ອນຂໍ້ມູນ"
                                        class="form-control @error('stock') is-invalid @enderror">
                                    @error('stock')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div> --}}
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="detail">ລາຍລະອຽດສິນຄ້າ</label>
                                    <div wire:ignore>
                                        <textarea class="form-control" id="note1" wire:model="note">{{$note}}</textarea>
                                    </div>
                                    @error('note')
                                    <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                </div>
                </form>
                <div class="modal-footer justify-content-between bg-light">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">ຍົກເລີກ</button>
                    <button wire:click="store" type="button" class="btn btn-success">ບັນທຶກ</button>
                </div>
            </div>
        </div>
    </div>
