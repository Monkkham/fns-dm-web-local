            <!--Foram add new-->
            <div class="col-md-4">
                <div class="card card-default">
                  <div class="card-header bg-pink">
                    <label>ເພີ່ມໃຫມ່ / ແກ້ໄຂ</label>
                  </div>
                  <form>
                      <div class="card-body">
                          <input type="hidden" wire:model="ID" value="{{$ID}}">
                          <div class="ro">
                            <div class="col-sm-12">
                              <label>ເລືອກປະເພດ</label>
                              <div class="form-group clearfix">
                                  <div class="icheck-success d-inline">
                                      <input type="radio" id="radioPrimary1" value="1" wire:model="type"
                                          checked>
                                      <label for="radioPrimary1">ລາຍຮັບ
                                      </label>
                                  </div>
                                  {{-- </div>
                              <div class="form-group clearfix"> --}}
                                  <div class="icheck-success d-inline">
                                      <input type="radio" id="radioPrimary2" value="2" wire:model="type">
                                      <label for="radioPrimary2">ລາຍຈ່າຍ
                                      </label>
                                  </div>
                              </div>
                              @error('type')
                                  <span style="color: red" class="error">{{ $message }}</span>
                              @enderror
                          </div>
                          </div>
                          <div class="row">
                              <div class="col-md-12">
                                <div class="form-group">
                                  <label>ຈ່າຍຄ່າ</label>
                                  <input wire:model="name" type="text" class="form-control @error('name') is-invalid @enderror" placeholder="ປ້ອນຂໍ້ມູນ">
                                  @error('name') <span style="color: red" class="error">{{ $message }}</span> @enderror
                                </div>
                              </div>
                              <div class="col-md-12">
                                <div class="form-group">
                                  <label>ເປັນເງິນ</label>
                                  <input wire:model="money" type="number" min="1" class="form-control @error('money') is-invalid @enderror" placeholder="ປ້ອນຂໍ້ມູນ">
                                  @error('money') <span style="color: red" class="error">{{ $message }}</span> @enderror
                                </div>
                              </div>
                          </div>
                      </div>
                      <div class="card-footer">
                          <div class="d-flex justify-content-between md-2">
                            @foreach ($rolepermissions as $items)
                            @if ($items->permissionname->name == 'action_notebook')
                                  <button type="button" wire:click="resetform" class="btn btn-warning">ຣີເຊັດ</button>
                                  <button type="button" wire:click="store" class="btn btn-success">ບັນທຶກ</button>
                                  @endif
                              @endforeach
                          </div>
                      </div>
                  </form>
                </div>
              </div>

@push('scripts')
    <script>
      window.addEventListener('show-modal-delete', event => {
          $('#modal-delete').modal('show');
      })
      window.addEventListener('hide-modal-delete', event => {
          $('#modal-delete').modal('hide');
      })
    </script>
@endpush