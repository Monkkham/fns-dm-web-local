<?php

namespace App\Http\Livewire\Backend\Reports;

use App\Models\Orders;
use Livewire\Component;
use App\Models\OrdersDetail;

class OrderReportContent extends Component
{
    public $generate_Number;
    public $start, $end;
    public $starts, $ends;
    public $created_at,$orderdetail_data = [],$orderdetail_sum_quantity,$orderdetail_sum_subtotal;
    public $employee_data,$supplier_data,$payment,$status,$payment_type;
    public function render()
    {
        $this->generate_Number = rand(10000000,99999999);
        $start = $this->starts;
        $end = date('Y-m-d', strtotime($this->ends . ' + ' . 1 . 'days'));
        $orders = Orders::select('*')
        ->where('status', 2)
            ->whereBetween('created_at', [$this->starts, $end])->get();
            $sum_total = Orders::select('total')
            ->where('status', 2)
            ->whereBetween('created_at', [$this->starts, $end])
            ->orderBy('id', 'desc')->sum('total');
        return view('livewire.backend.reports.order-report-content',compact('orders','sum_total'))->layout('layouts.backend.base');
    }
    public function ShowDetail($ids)
    {
        $this->dispatchBrowserEvent('show-modal-detail');
        $data = Orders::find($ids);
        $this->ID = $data->id;
        $this->employee_data = $data->employee;
        $this->supplier_data = $data->supplier;
        $this->payment = $data->payment;
        $this->payment_type = $data->payment_type;
        $this->status = $data->status;
        $this->created_at = $data->created_at;
        $this->orderdetail_data = OrdersDetail::where('orders_id',$this->ID)->get();
        $this->orderdetail_sum_quantity = OrdersDetail::where('orders_id',$this->ID)->sum('quantity');
        $this->orderdetail_sum_subtotal = OrdersDetail::where('orders_id',$this->ID)->sum('subtotal');
    }
    public function sub()
    {
       if(!empty($this->start || $this->end))
       {
        $this->starts = $this->start;
        $this->ends = $this->end;
       }else{
        $this->emit('alert', ['type' => 'error', 'message' => 'ກະລຸນາເລືອກວັນທີ່ຫາວັນທີ່ກ່ອນ!']);
       }
    }
}
