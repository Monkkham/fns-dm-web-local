<?php

namespace App\Http\Livewire\Backend\Reports;

use App\Models\Orders;
use Livewire\Component;
use App\Models\NoteBook;

class ExpendReportContent extends Component
{
    public $start, $end;
    public $starts, $ends;
    public $generate_Number;
    public function render()
    {
        $this->generate_Number = rand(10000000,99999999);
        $start = $this->starts;
        $end = date('Y-m-d', strtotime($this->ends . ' + ' . 1 . 'days'));
        $expends = NoteBook::select('*')
            ->where('type', 2)
            ->whereBetween('created_at', [$this->starts, $end])->get();
        $sum_notebook = NoteBook::select('money')
            ->where('type', 2)
            ->whereBetween('created_at', [$this->starts, $end])
            ->orderBy('id', 'desc')->sum('money');
        $sum_orders = Orders::select('total')
            ->where('status', 2)
            ->whereBetween('created_at', [$this->starts, $end])
            ->orderBy('id', 'desc')->sum('total');
        return view('livewire.backend.reports.expend-report-content', compact('expends', 'sum_notebook', 'sum_orders'))->layout('layouts.backend.base');
    }
    public function sub()
    {
        if (!empty($this->start || $this->end)) {
            $this->starts = $this->start;
            $this->ends = $this->end;
        } else {
            $this->emit('alert', ['type' => 'error', 'message' => 'ກະລຸນາເລືອກວັນທີ່ຫາວັນທີ່ກ່ອນ!']);
        }
    }
}
