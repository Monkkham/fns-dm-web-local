    <!-- /.modal-edit -->
    <div wire:ignore.self class="modal fade" id="modal-edit">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header bg-light">
                    <h5 class="modal-title"><i class="fa fa-edit text-warning"></i> ແກ້ໄຂ</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <form>
                        <div class="container">
                            <div class="avatar-upload">
                                <div class="avatar-edit">
                                    <input type='file' wire:model="image" id="imageUpload2"
                                        accept=".png, .jpg, .jpeg" />
                                    <label for="imageUpload2"></label>
                                </div>
                                @if ($image)
                                    <div class="avatar-preview">
                                        <img id="imagePreview2" src="{{ $image->temporaryUrl() }}" alt="" width="120px;">
                                    </div>
                                @else
                                    @if ($newimage)
                                        <div class="avatar-preview">
                                            <img id="imagePreview2" src="{{ asset('employee') }}/{{ $newimage }}"
                                                alt="" width="120px;">
                                        </div>
                                    @else
                                        <div class="avatar-preview">
                                            <div id="imagePreview2"
                                                style="background-image: url({{ asset('logo/noimage.png') }});">
                                            </div>
                                        </div>
                                    @endif
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <label>ເລືອກເພດ</label>
                                <div class="form-group clearfix">
                                    <div class="icheck-success d-inline">
                                        <input type="radio" id="radioPrimary1" value="1" wire:model="gender"
                                            checked>
                                        <label for="radioPrimary1">ຍິງ
                                        </label>
                                    </div>
                                    {{-- </div>
                                <div class="form-group clearfix"> --}}
                                    <div class="icheck-success d-inline">
                                        <input type="radio" id="radioPrimary2" value="2" wire:model="gender">
                                        <label for="radioPrimary2">ຊາຍ
                                        </label>
                                    </div>
                                </div>
                                @error('gender')
                                    <span style="color: red" class="error">{{ $message }}</span>
                                @enderror
                            </div>
                        {{-- </div>
                        <div class="row"> --}}
                            <div class="col-sm-6">
                                <label>ເລືອກສະຖານະ</label>
                                <div class="form-group clearfix">
                                    <div class="icheck-success d-inline">
                                        <input type="radio" id="radioPrimary3" value="1" wire:model="status"
                                            checked>
                                        <label for="radioPrimary3">ໂສດ
                                        </label>
                                    </div>
                                    {{-- </div>
                                <div class="form-group clearfix"> --}}
                                    <div class="icheck-success d-inline">
                                        <input type="radio" id="radioPrimary4" value="2" wire:model="status">
                                        <label for="radioPrimary4">ມີແຟນ
                                        </label>
                                    </div>
                                    <div class="icheck-success d-inline">
                                        <input type="radio" id="radioPrimary5" value="3" wire:model="status">
                                        <label for="radioPrimary5">ແຕ່ງງານ
                                        </label>
                                    </div>
                                </div>
                                @error('gender')
                                    <span style="color: red" class="error">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>ຊື່</label>
                                    <input wire:model="name" type="text" placeholder="ປ້ອນຂໍ້ມູນ"
                                        class="form-control @error('name') is-invalid @enderror">
                                    @error('name')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>ນາມສະກຸນ</label>
                                    <input wire:model="lastname" type="text" placeholder="ປ້ອນຂໍ້ມູນ"
                                        class="form-control @error('lastname') is-invalid @enderror">
                                    @error('lastname')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>ເບີໂທ</label>
                                    <input wire:model="phone" type="tel" placeholder="ປ້ອນຂໍ້ມູນ"
                                        class="form-control @error('phone') is-invalid @enderror">
                                    @error('phone')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>ອີເມວ</label>
                                    <input wire:model="email" type="email" placeholder="ປ້ອນຂໍ້ມູນ"
                                        class="form-control @error('email') is-invalid @enderror">
                                    @error('email')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>ທີ່ຢູ່</label>
                                    <input wire:model="address" type="text" placeholder="ປ້ອນຂໍ້ມູນ"
                                        class="form-control @error('address') is-invalid @enderror">
                                    @error('address')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>ວດປ ເກີດ</label>
                                    <input wire:model="birthday" type="date" placeholder="ປ້ອນຂໍ້ມູນ"
                                        class="form-control @error('birthday') is-invalid @enderror">
                                    @error('birthday')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>ເລືອກສິດທິ</label>
                                    <select wire:model="roles_id" class="form-control">
                                        <option value="" selected>ເລືອກ</option>
                                        @foreach ($roles as $item)
                                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('roles_id')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>ລະຫັດຜ່ານ</label>
                                    <input wire:model="password" type="password" id="password" placeholder="********"
                                        class="form-control @error('address') is-invalid @enderror">
                                        @error('password')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                    <input type="checkbox" id="eye"
                                        onclick="show_password()">&nbsp;<label>ສະເເດງ</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>ຍືນຍັນລະຫັດຜ່ານ</label>
                                    <input wire:model="con_password" type="password" id="con_password"
                                        placeholder="********" class="form-control">
                                </div>
                                {{-- @if (session::has('no_match_password'))
                                    <span style="color: red" class="error">{{ session::get('no_match_password') }}</span>
                                @endif --}}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group" wire:ignore>
                                    <label>ເລືອກແຂວງ</label>
                                    <select wire:model="province_id" id="select2" class="form-control
                                     @error('province_id') is-invalid @enderror">
                                        <option value="" selected>ເລືອກ</option>
                                        @foreach ($provinces as $item)
                                            <option value="{{ $item->id }}">{{ $item->name_la }}</option>
                                        @endforeach
                                    </select>
                                    @error('province_id')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>ເລືອກເມືອງ</label>
                                    <select wire:model="district_id" class="form-control
                                     @error('district_id') is-invalid @enderror">
                                        <option value="" selected>ເລືອກ</option>
                                        @foreach ($districts as $item)
                                            <option value="{{ $item->id }}">{{ $item->name_la }}</option>
                                        @endforeach
                                    </select>
                                    @error('district_id')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>ເລືອກບ້ານ</label>
                                    <select wire:model="village_id" class="form-control
                                     @error('village_id') is-invalid @enderror">
                                        <option value="" selected>ເລືອກ</option>
                                        @foreach ($villages as $item)
                                            <option value="{{ $item->id }}">{{ $item->name_la }}</option>
                                        @endforeach
                                    </select>
                                    @error('village_id')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                </div>
                </form>
                <div class="modal-footer justify-content-between bg-light">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">ຍົກເລີກ</button>
                    <button wire:click="update" type="button" class="btn btn-success">ບັນທຶກ</button>
                </div>

            </div>
        </div>
    </div>