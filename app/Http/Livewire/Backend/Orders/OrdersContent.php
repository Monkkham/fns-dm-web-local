<?php

namespace App\Http\Livewire\Backend\Orders;
use Livewire\Component;
use App\Models\Supplier;
use App\Models\ProductType;
use Livewire\WithPagination;
use Illuminate\Support\Facades\DB;
use App\Models\Orders;
use Gloudemans\Shoppingcart\Facades\Cart;
use App\Models\Products;
use App\Models\OrdersDetail;

class OrdersContent extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $no1 = 1;
    public $search;
    public $cartData;
    public $cartCount;
    public $cartTotal;
    public $product_type_id;
    public $supplier,$supplier_id;
    public $suppiler_data;
    public function render()
    {
        $suppliers = Supplier::all();
        if(!empty($this->supplier_id)){
                $this->suppiler_data = Supplier::orderBy('id','desc')
                ->where('id', $this->supplier_id)->first();
        }
        $product_type = ProductType::orderBy('id','desc')->get();
        if(!empty($this->product_type_id)){
            if(!empty($this->search)){
                $products = Products::orderBy('id','desc')
                ->where('product_type_id', $this->product_type_id)
                ->where(function($query){
                    $query->where('code', 'like', '%' . $this->search . '%')
                    ->orWhere('name', 'like', '%' . $this->search . '%');
                })->paginate(12);
            }else{
                $products = Products::orderBy('id','desc')
                ->where('product_type_id', $this->product_type_id)
                ->paginate(12);
            }
        }else{
            if(!empty($this->search)){
                $products = Products::orderBy('id','desc')
                ->where(function($query){
                    $query->where('code', 'like', '%' . $this->search . '%')
                    ->orWhere('name', 'like', '%' . $this->search . '%');
                })->paginate(12);
            }else{
                $products = Products::orderBy('id','desc')
                ->paginate(12);
            }
        }
        // ================
        // if ($this->search != null) {
        //     $products = Products::where('name', 'like', '%' . $this->search . '%')->paginate(12);
        // } else {
        //     $products = Products::orderBy('id','desc')->paginate(12);
        // }
        $this->cartData = Cart::content();
        $this->cartCount = Cart::content()->count();
        $this->cartTotal = Cart::subTotal();
        return view('livewire.backend.orders.orders-content',compact('products','product_type','suppliers'))->layout('layouts.backend.base');
    }
    // add to cart
    public function addCart($p_id)
    {
        $products = Products::findOrFail($p_id);
        $itemsId = $products->id;
        $itemsName = $products->name;
        $itemsQty = 1;
        $itemsPrice = (float)$products->buy_price;
        Cart::add($itemsId, $itemsName, $itemsQty, $itemsPrice);
        $this->emit('alert', ['type' => 'success', 'message' => 'ເພີ່ມໃສ່ກະຕ່າເເລ້ວ!']);
    }
    // update cart
    public $rowId, $qty;
    public function updateCart($qty, $rowId, $status)
    {
        if ($status == 1) {
            $this->rowId = $rowId;
            $this->qty = $qty + 1;
            Cart::update($this->rowId, $this->qty);
            $this->emit('alert', ['type' => 'success', 'message' => 'plus+!']);
            return;
        } else {
            $this->rowId = $rowId;
            $this->qty = $qty - 1;
            Cart::update($this->rowId, $this->qty);
            $this->emit('alert', ['type' => 'error', 'message' => 'delete-!']);
            return;
        }
    }
    // remove cart
    public function removeCart()
    {
        Cart::destroy();
        $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບລ້າງກະຕ່າ!']);
        return;
    }
    // show cart with modal
    public function _checkout()
    {
        if ($this->cartCount == 0) {
            $this->emit('alert', ['type' => 'error', 'message' => 'ກະລຸນາເລືອກສິນຄ້າກ່ອນ!']);
            return;
        } else {
            $this->showaddform();
        }
    }
    // sale save
    public $suplyer;
    public function _sale()
    {
        $this->validate([
            'suplyer'=>'required',
        ],[
            'suplyer.required'=>'ເລືຶອກຂໍ້ມູນກ່ອນ!',
        ]);
        DB::beginTransaction();
        // try {
            $sale = new Orders();
            $sale->code = 'OD' . rand(100000, 999999);
            $sale->total = str_replace(",", "", $this->cartTotal);
            $sale->status = 1;
            $sale->payment = 1;
            $sale->supplier_id = $this->supplier_id;
            $sale->employee_id = auth()->user()->id;
            $sale->note = 'ສັ່ງຊື້ສິນຄ້ານຳຜູ້ສະຫນອງ';
            $sale->save();
            $cartData = Cart::content();
            foreach ($cartData as $item) {
                if (isset($item->id)) {
                    $saleItem = new OrdersDetail();
                    $saleItem->orders_id = $sale->id;
                    $saleItem->product_id = $item->id;
                    $saleItem->quantity = $item->qty;
                    $saleItem->subtotal = $item->qty * $item->price;
                    $saleItem->save();
                    DB::commit();
                }
            }
            DB::commit();
            $this->closeaddform();
            Cart::destroy();
            $this->resetForm();
            $this->dispatchBrowserEvent('swal:confirm', [
                'type' => 'success',  
                'message' => 'ສ້າງບິນສັ່ງຊື້ສິນຄ້າສຳເລັດ!', 
                'text' => 'ຂໍຂອບໃຈ'
            ]);
            $id = $sale->id;
            return redirect()->route('backend.printbill', ['slug_id' => $id]);
        // return redirect()->route('backend.printbill');
            // $this->emit('alert', ['type' => 'success', 'message' => 'orderded!']);
            // return redirect()->route('sale-pos-print', ['id' => $id]);
        // } catch (\Exception $e) {
        //     DB::rollback();
        //     $this->emit('alert', ['type' => 'error', 'message' => 'something wrong!']);
        //     $this->resetForm();
        // }
    }
    // reset supplier
    public function resetForm()
    {
        $this->suplyer = '';
    }
    // show form modal

    protected function showaddform()
    {
        $this->dispatchBrowserEvent('showforma');
    }
    protected function closeaddform()
    {
        $this->dispatchBrowserEvent('closeforma');
    }
}
