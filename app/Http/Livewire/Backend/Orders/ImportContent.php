<?php

namespace App\Http\Livewire\Backend\Orders;

use App\Models\Orders;
use Livewire\Component;
use App\Models\Products;
use App\Models\Supplier;
use App\Models\OrdersDetail;
use Livewire\WithPagination;

class ImportContent extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $search;
    public $icomeOrderItems;
    public $ID,$supplier,$supplier_data,$employee_data,$payment_type;
    public $orderdetail_data = [];
    public $orderdetail_sum_quantity,$orderdetail_sum_subtotal,$payment,$status;
    public function render()
    {
        $imports_orders = Orders::orderBy('id', 'desc')->where('code', 'like', '%' . $this->search . '%')->paginate(5);
        return view('livewire.backend.orders.import-content', compact('imports_orders'))->layout('layouts.backend.base');
    }
        // ================== ສະເເດງລາຍລະອຽດໂຄງການ ======================= //
        public function show_bill($ids)
        {
            $this->dispatchBrowserEvent('show-modal-bill');
            $data = Orders::find($ids);
            $this->ID = $data->id;
            $this->employee_data = $data->employee;
            $this->supplier_data = $data->supplier;
            $this->payment = $data->payment;
            $this->payment_type = $data->payment_type;
            $this->status = $data->status;
            $this->created_at = $data->created_at;
            $this->orderdetail_data = OrdersDetail::where('orders_id',$this->ID)->get();
            $this->orderdetail_sum_quantity = OrdersDetail::where('orders_id',$this->ID)->sum('quantity');
            $this->orderdetail_sum_subtotal = OrdersDetail::where('orders_id',$this->ID)->sum('subtotal');
        }
    // show order
    public $code, $subtotal, $count_product,$product_id,$orders_id, $created_at, $total, $quantity, $orderId;
    public $importOrderItems;
    public function showorder($ids)
    {
        $this->showaddform();
        $importOrder = Orders::find($ids);
        $this->code = $importOrder->code;
        $this->created_at = $importOrder->created_at;
        $this->total = $importOrder->total_money;
        $this->count_product = OrdersDetail::where('orders_id', $ids)->count();
        $this->subtotal = OrdersDetail::where('orders_id', $ids)->sum('subtotal');
        $this->importOrderItems = OrdersDetail::where('orders_id', $ids)->get();
        $this->orderId = $ids;
    }
    public function DeleteOrderItem($ids)
    {
        $data = OrdersDetail::find($ids);
        $data->delete();
        $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບລາຍການສິນຄ້າ!']);
        return;
    }

    // update product quantity
    public function updateStock($orders_id, $product_id, $status)
    {
        if ($status == 1) {
            $order_detail = OrdersDetail::where('product_id', $product_id)->where('orders_id', $orders_id)->first();
            $order_detail->quantity = $order_detail->quantity + 1;
            $order_detail->save();
            $this->emit('alert', ['type' => 'success', 'message' => 'ເພີ່ມຂື້ນ+!']);
            return;
        } else {
            $order_detail = OrdersDetail::where('product_id', $product_id)->where('orders_id', $orders_id)->first();
            $order_detail->quantity = $order_detail->quantity - 1;
            $order_detail->save();
            $this->emit('alert', ['type' => 'error', 'message' => 'ລຶບອອກ-!']);
            return;
        }
    }

    // update order
    public function icomeOk($orderId, $status)
    {
        $importOrder = Orders::findOrFail($orderId);
        if($status == 3){
        if ($importOrder->status != 2) {
            $importOrder->status = 2;
            $importOrder->payment = 2;
            $importOrder->payment_type = $this->payment_type;
            $importOrder->save();
            $importOrderItems = OrdersDetail::where('orders_id', $orderId)->get();
            foreach ($importOrderItems as $item) {
                $product = Products::findOrFail($item->product_id);
                $product->stock = $product->stock + $item->quantity;
                $product->save();
            }
            // $ids = $orderId;
            // return redirect()->route('sale-pos-print', ['id' => $ids]);
            $this->dispatchBrowserEvent('swal:confirm', [
                'type' => 'success',
                'message' => 'ນຳເຂົ້າສິນຄ້າສຳເລັດ!',
                'text' => 'ຂໍຂອບໃຈ',
            ]);
        } else {
            $this->emit('alert', ['type' => 'error', 'message' => 'ມີບາງຢ່າງຜິດພາດ!']);
        }
        }else{
            $ids = $this->orderId;
            $data = Orders::find($ids);
            $data->status = 3;
            $data->save();
            $this->dispatchBrowserEvent('hide-modal-delete');
            $this->dispatchBrowserEvent('swal', [
                'title' => 'ລາຍການສັ່ງຊື້ຖືກຍົກເລີກ!',
                'icon'=>'success',
                'iconColor'=>'green',
            ]);
        }
        $this->closeaddform();
    }
    // show form modal
    protected function showaddform()
    {
        $this->dispatchBrowserEvent('showforma');
    }
    protected function closeaddform()
    {
        $this->dispatchBrowserEvent('closeforma');
    }
}
