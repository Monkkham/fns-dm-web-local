<?php
return [
'title'=>'Dollar phamezub',
'laos'=>'Laos',
'english'=>'English',
'user_and_role'=>'Role and User',
'role'=>'Roles',
'users'=>'Users',
'dashboard'=>'Dashboard',
'add_data'=>'Add Data',
'tel_user'=>'Phone',
'password'=>'Password',
'login'=>'Login',
'welcome'=>'Welcome',
'logout'=>'Logout',
'settings'=>'Settings',
'branch'=>'Branch',
'village'=>'Village',
'district'=>'District',
'province'=>'Province',
'group1'=>'Group Menu1'
];
