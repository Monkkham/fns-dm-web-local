<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tables extends Model
{
    use HasFactory;
    protected $table = "tables";
    protected $fillable =
     ['id',
     'zones_id',
     'code',
     'chiar_qty',
    'status', // 0 = ຫວ່າງ , 1 = ບໍ່ຫວ່າງ
    'created_at',
    'updated_at'];
    public function zones()
    {
        return $this->belongsTo('App\Models\Zones','zones_id','id');
    }
}
