<div wire:poll>
    {{-- ======================================== name page ====================================================== --}}
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h6><i class="fas fa-users"></i> ສັ່ງຈອງ</h6>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">ຫນ້າຫຼັກ</a></li>
                        <li class="breadcrumb-item active">ສັ່ງຈອງ</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!--List users- table table-bordered table-striped -->
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h6><i class="fas fa-users"></i> ລາຍການລູກຄ້າສັ່ງຈອງ</h6>
                                            {{-- @foreach ($rolepermissions as $items)
                                            @if ($items->permissionname->name == 'action_employee') --}}
                                            {{-- <a wire:click="create" class="btn btn-primary btn-sm"
                                                href="javascript:void(0)"><i class="fa fa-plus-circle"></i>
                                                ເພີ່ມໃຫມ່</a> --}}
                                            {{-- @endif
                                             @endforeach --}}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">

                                </div>
                                <div class="row">
                                    <div class="col-md-9">
                                    </div>
                                    <div class="input-group input-group-sm" style="width: 240px;">
                                        <input wire:model="search" type="text" class="form-control"
                                            placeholder="ຄົ້ນຫາ">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead class="bg-pink">
                                        <tr class="text-center">
                                            <th>ລຳດັບ</th>
                                            <th>ລະຫັດບິນ</th>
                                            <th>ເງິນມັດຈຳ</th>
                                            <th>ລູກຄ້າ</th>
                                            <th>ເພດ</th>
                                            <th>ເບີໂທ</th>
                                            <th>ທຸລະກຳ</th>
                                            <th>ວ.ດ.ປ ຈອງ</th>
                                            <th>ສະຖານະ</th>
                                            @foreach ($rolepermissions as $items)
                                            @if ($items->permissionname->name == 'action_preorder')
                                            <th>ຈັດການ</th>
                                            @endif
                                            @endforeach
                                        </tr>
                                    </thead>
                                    @php
                                        $num = 1;
                                    @endphp
                                    <tbody>

                                        @foreach ($preorders as $item)
                                            <tr class="text-center">
                                                <td>{{ $num++ }}</td>
                                                <td><a wire:click="ShowBill({{ $item->id }})"
                                                        href="#">{{ $item->code }}</a></td>
                                                <td class="text-bold">{{ number_format($item->total) }} ₭</td>
                                                <td>
                                                    @if (!empty($item->customer))
                                                        {{ $item->customer->name }} {{ $item->customer->lastname }}
                                                    @endif
                                                </td>
                                                <td>
                                                    @if (!empty($item->customer))
                                                        @if ($item->customer->gender == 1)
                                                            <b class="text-success">ຍິງ</b>
                                                        @elseif($item->customer->gender == 2)
                                                            <b class="text-info">ຊາຍ</b>
                                        @endif
                                        @endif
                                        </td>
                                        <td>
                                            @if (!empty($item->customer))
                                                {{ $item->customer->phone }}
                                            @endif
                                        </td>
                                        <td>
                                            @if ($item->payment == 1)
                                                <button type="button" wire:click="show_onepay({{ $item->id }})"
                                                    class="btn btn-sm btn-outline-danger"><i
                                                        class="fa fa-credit-card"></i> OnePay</button>
                                            @elseif($item->payment == 0)
                                                <p class="badge badge-success p-2"><i class="fa fa-check-circle"></i>
                                                    ຊຳລະເເລ້ວ</p>
                                            @endif
                                        </td>
                                        <td>{{ date('d/m/Y', strtotime($item->updated_at)) }}</td>
                                        <td>
                                            @if ($item->status == 1)
                                                <p class="bg-success text-center rounded"><i
                                                        class="fas fa-plus-circle"></i>
                                                    ໃຫມ່ <span
                                                        class="spinner-grow mb-1 spinner-grow-sm text-white text-center"
                                                        role="status" aria-hidden="true"></span></p>
                                            @elseif($item->status == 2)
                                                <p class="bg-success text-center text-white rounded"><i
                                                        class="fa fa-check-circle"></i> ນຳເຂົ້າສຳເລັດ</p>
                                            @elseif($item->status == 0)
                                                <p class="bg-danger text-center text-white rounded"><i
                                                        class="fas fa-times-circle"></i> ຖືກຍົກເລີກ</p>
                                            @endif
                                        </td>
                                        {{-- <td>{{ date('d-m-Y', strtotime($item->created_at)) }}</td> --}}
                                        @foreach ($rolepermissions as $items)
                                                @if ($items->permissionname->name == 'action_preorder')
                                        <td>
                                            @if ($item->payment != 1 && $item->status != 0)
                                                <button wire:click="confirm_preorder({{ $item->id }})" type="button"
                                                    class="btn btn-primary btn-sm"><i
                                                        class="fas fa-check-circle"></i> ອານຸມັດ
                                                </button>
                                            @endif
                                        </td>
                                        @endif
                                                @endforeach
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <div class="float-right">
                                    {{ $preorders->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('livewire.backend.pre-orders.check-onepay')
    @include('livewire.backend.pre-orders.preorder-bill')
</div>
@push('scripts')
    <script>
        //============ onepay ============= //
        window.addEventListener('show-onepay', event => {
            $('#onepay').modal('show');
        });
        window.addEventListener('hide-onepay', event => {
            $('#onepay').modal('hide');
        });
        window.addEventListener('show-modal-bill', event => {
            $('#modal-bill').modal('show');
        });
        window.addEventListener('closeforma', event => {
            $('#modala').modal('hide');
        });
        window.addEventListener('showforma', event => {
            $('#modala').modal('show');
        });
        window.addEventListener('closeforma', event => {
            $('#modala').modal('hide');
        });
        $(function() {
            $('.select2').select2()
            $('#suplyerId').on('change', function(e) {
                let data = $(this).val();
                @this.set('suplyer', data);
            });
        });
    </script>
@endpush
