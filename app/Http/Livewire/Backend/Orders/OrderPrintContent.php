<?php

namespace App\Http\Livewire\Backend\Orders;

use App\Models\Orders;
use Livewire\Component;
use App\Models\OrdersDetail;

class OrderPrintContent extends Component
{
    public $search;
    public $icomeOrderItems;
    public $ID,$supplier,$supplier_data,$employee_data;
    public $orderdetail_data = [];
    public $orderdetail_sum_quantity,$orderdetail_sum_subtotal,$payment,$status,$data;
    public function mount($slug_id)
    {
        $this->data = Orders::find($slug_id);
        $this->ID = $this->data->id;
        $this->employee_data = $this->data->employee;
        $this->supplier_data = $this->data->supplier;
        $this->payment = $this->data->payment;
        $this->status = $this->data->status;
        $this->created_at = $this->data->created_at;
        $this->orderdetail_data = OrdersDetail::where('orders_id',$this->ID)->get();
        $this->orderdetail_sum_quantity = OrdersDetail::where('orders_id',$this->ID)->sum('quantity');
        $this->orderdetail_sum_subtotal = OrdersDetail::where('orders_id',$this->ID)->sum('subtotal');
    }
    public function render()
    {
        return view('livewire.backend.orders.order-print-content')->layout('layouts.backend.base');
    }
}
