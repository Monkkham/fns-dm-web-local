<!-- Main content -->
<div wire:ignore.self class="modal fade" id="modal-bill">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-light">
                <h5 class="modal-title"><i class="fas fa-file"></i> <b>ລາຍລະອຽດການສັ່ງຊື້</b></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="invoice p-3 mb-3">
                    <!-- title row -->
                    <div class="row">
                        <div class="col-12">
                            <h4>
                                <small class="float-left">
                                    <p class="brand-link">
                                        <img src="https://img.freepik.com/free-vector/detailed-chef-logo-template_23-2148987940.jpg?w=740&t=st=1686808310~exp=1686808910~hmac=49bda835bfdaab10b4789fc560ae7d2e43d678d543c0c23a7c4ef2b33a313dc8" alt="AdminLTE Logo"
                                            class="brand-image img-circle elevation-3" style="opacity: .8"> ຮ້ານ: ອາຫານ
                                        ດາວອັງຄານ
                                    </p>
                                </small>
                                <small class="float-right text-sm"><b>ວັນທີ່:</b>
                                    {{ date('d-m-Y', strtotime($created_at)) }} <br> <b>ເວລາ:</b>
                                    {{ date('H:i:s', strtotime($created_at)) }}</small>
                            </h4>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- info row -->
                    <div class="row invoice-info">
                        <div class="col-sm-6 invoice-col">
                            <address>
                                <strong><i class="fas fa-user-tie"></i> ພະນັກງານ</strong><br>
                                @if (!empty($this->employee_data))
                                    <b>ຊື່:</b> {{ $this->employee_data->name }} {{ $this->employee_data->lastname }}
                                    <br>
                                    <b>ເບີໂທ:</b> {{ $this->employee_data->phone }} <br>
                                    <b>ອີເມວ:</b> {{ $this->employee_data->email }} <br>
                                    <b>ທີ່ຢູ່:</b> {{ $this->employee_data->village->name_la }},
                                    {{ $this->employee_data->district->name_la }},
                                    {{ $this->supplier_data->province->name_la }} <br>
                                @endif
                                {{-- <strong><i class="fas fa-home"></i> ສຳນັກງານໃຫຍ່</strong><br>
                                <b>ຕັ້ງຢູ່:</b> ບ້ານນາທ່າງອນ ເມືອງໄຊທານີ ນະຄອນຫລວງວຽງຈັນ<br>
                                <b>ໂທ/ແອັບ:</b> 020 55555555<br>
                                <b>Email:</b> info@gmail.com --}}
                            </address>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-6 invoice-col">
                            <address>
                                <strong><i class="fas fa-user-tie"></i> ຜູ້ສະຫນອງ</strong><br>
                                @if (!empty($this->supplier_data))
                                    <b>ຊື່:</b> {{ $this->supplier_data->name }} {{ $this->supplier_data->lastname }}
                                    <br>
                                    <b>ເບີໂທ:</b> {{ $this->supplier_data->phone }} <br>
                                    <b>ອີເມວ:</b> {{ $this->supplier_data->email }} <br>
                                    <b>ທີ່ຢູ່:</b> {{ $this->supplier_data->village->name_la }},
                                    {{ $this->supplier_data->district->name_la }},
                                    {{ $this->supplier_data->province->name_la }} <br>
                                @endif
                            </address>
                        </div>
                    </div>
                    <!-- /.row -->

                    <!-- Table row -->
                    <div class="row">
                        <div class="col-12 table-responsive">
                            <table class="table table-striped">
                                <thead class="bg-pink">
                                    <tr>
                                        <th>ລຳດັບ</th>
                                        <th>ສິນຄ້າ</th>
                                        <th>ປະເພດ</th>
                                        <th>ລາຄາ</th>
                                        <th>ຈຳນວນ</th>
                                        <th>ເປັນເງິນ</th>
                                    </tr>
                                </thead>
                                @php
                                    $num = 1;
                                @endphp
                                <tbody>
                                    @foreach ($this->orderdetail_data as $item)
                                        @if (!empty($item->products))
                                            <tr>
                                                <td>{{ $num++ }}</td>
                                                <td class="text-bold">{{ $item->products->name }}</td>
                                                <td>{{ $item->products->product_type->name }}</td>
                                                <td>{{ number_format($item->products->buy_price) }} ₭</td>
                                                <td>{{ $item->quantity }}</td>
                                                <td>{{ number_format($item->subtotal) }} ₭</td>
                                            </tr>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                    <div class="row">
                        <div class="col-5">
                            <div class="table-responsive">
                                <table class="table">
                                    <tr>
                                        <th style="width:50%">ລວມຈຳນວນ:</th>
                                        <td class="text-sm">{{ $this->orderdetail_sum_quantity }} ລາຍການ
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>ລວມເປັນເງິນ:</th>
                                        <td class="text-sm">{{ number_format($this->orderdetail_sum_subtotal) }} ₭
                                    </tr>
                                    <tr>
                                        <th>ຊຳລະເງິນ:</th>
                                        @if ($this->payment == 1)
                                            <td class="text-danger"><i class="fas fa-hand-holding-usd"></i> ບໍ່ທັນຊຳລະ</td>
                                        @elseif($this->payment == 2)
                                            <td class="text-success"><i class="fas fa-check-circle"></i> ຊຳລະເເລ້ວ</td>
                                        @endif
                                    </tr>
                                    <tr>
                                        <th>ປະເພດຊຳລະ:</th>
                                        @if ($this->payment_type == 1)
                                            <td class="text-danger"><i class="fas fa-hand-holding-usd"></i> ເງິນສົດ</td>
                                        @elseif($this->payment_type == 2)
                                            <td class="text-success"><i class="fas fa-money"></i> ເງິນໂອນ</td>
                                        @endif
                                    </tr>
                                    <tr>
                                        <th>ສະຖານະ:</th>
                                        @if ($this->status == 1)
                                            <td class="text-warning"><i class="fas fa-search"></i> ລໍຖ້າກວດສອບ</td>
                                        @elseif($this->status == 2)
                                            <td class="text-success"><i class="fas fa-check-circle"></i> ນຳເຂົ້າສຳເລັດ</td>
                                            @elseif($this->status == 3)
                                            <td class="text-danger"><i class="fas fa-times-circle"></i> ຖືກຍົກເລີກ</td>
                                        @endif
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.invoice -->
