<?php

namespace App\Http\Livewire\Backend\Reports;

use Livewire\Component;
use App\Models\NoteBook;
use App\Models\PreOrder;

class IncomeReportContent extends Component
{
    public $generate_Number;
    public $start, $end;
    public $starts, $ends;
    public function render()
    {
        $this->generate_Number = rand(10000000,99999999);
        $start = $this->starts;
        $end = date('Y-m-d', strtotime($this->ends . ' + ' . 1 . 'days'));
        $incomes = NoteBook::select('*')
            ->where('type',1)
            ->whereBetween('created_at', [$this->starts, $end])->get();
        $sum_total = NoteBook::select('money')
            ->where('type',1)
            ->whereBetween('created_at', [$this->starts, $end])
            ->orderBy('id', 'desc')->sum('money');
            $sum_preorder = PreOrder::select('total')
            ->where('status',3)
            ->whereBetween('created_at', [$this->starts, $end])
            ->orderBy('id', 'desc')->sum('total');
        return view('livewire.backend.reports.income-report-content',compact('incomes','sum_total','sum_preorder'))->layout('layouts.backend.base');
    }
    public function sub()
    {
       if(!empty($this->start || $this->end))
       {
        $this->starts = $this->start;
        $this->ends = $this->end;
       }else{
        $this->emit('alert', ['type' => 'error', 'message' => 'ກະລຸນາເລືອກວັນທີ່ຫາວັນທີ່ກ່ອນ!']);
       }
    }
}
