<div wire:poll>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h6><i class="fas fa-chart-line"></i>
                        ລາຍງານ
                        <i class="fa fa-angle-double-right"></i>
                        ຂໍ້ມູນສິນຄ້າ
                    </h6>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">ຫນ້າຫຼັກ</a></li>
                        <li class="breadcrumb-item active">ຂໍ້ມູນສິນຄ້າ</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <input type="date" wire:model="start" class="form-control">
                                    </div>
                                </div><!-- end div-col -->
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <input type="date" wire:model="end" class="form-control">
                                    </div>
                                </div><!-- end div-col -->
                                @foreach ($rolepermissions as $items)
                                @if ($items->permissionname->name == 'action_report_product')
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <button wire:click="sub()" class="btn btn-primary"><i
                                                class="fas fa-file-pdf"></i> ສະເເດງ</button>
                                        <button class="btn btn-info" id="print"><i class="fas fa-print"></i>
                                            ປິ່ຣນ</button>
                                    </div>
                                </div><!-- end div-col -->
                                @endif
                                @endforeach
                            </div><!-- end div-row -->
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="right_content">
                                        <div class="row">
                                            <div class="col-md-12 text-center">
                                                <h6>ສາທາລະນະລັດ ປະຊາທິປະໄຕ ປະຊາຊົນລາວ ສັນຕິພາບ ເອກະລາດ</h6>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-center">
                                                <h6>ປະຊາທິປະໄຕ ເອກະພາບ ວັດທະນາ ຖາວອນ</h6>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-center">
                                                ===========***===========
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3 text-center">
                                                <img src="{{ asset('logo/logo.jpg') }}"
                                                    class="brand-image-xl img-circle elevation-2" height="80"
                                                    width="80">
                                            </div>
                                            <div class="col-md-3"></div>
                                            <div class="col-md-6 text-right">
                                                <h6>ເລກທີ: {{ $this->generate_Number }}</h6>
                                                <h6>ວັນທີ່ພິມ: {{ date('d/m/Y') }}</h6>
                                                <h6>ເວລາ: {{ date('H:i:s') }}</h6>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <h5 class="text-center">ຮ້ານ ອາຫານ ດາວອັງຄານ</h5>
                                                <h6 class="text-sm"><i class="fas fa-phone-alt"></i> ຕິດຕໍ່:
                                                    020 96378460
                                                    <h6 class="text-sm"><i class="fas fa-envelope"></i> ອີເມວ:
                                                        daoangkhan@gmail.com
                                                        <h6 class="text-sm"><i class="fas fa-store-alt"></i> ທີ່ຕັ້ງ:
                                                            ນາທົ່ມ, ເມືອງໄຊທານີ, ນະຄອນຫລວງວຽງຈັນ</h6>
                                            </div>
                                            <div class="col-md-3"></div>
                                            <div class="col-md-6">

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-center">
                                                <h4><u><b>ລາຍງານ-ຂໍ້ມູນສິນຄ້າ</b></u></h4>
                                                <h4><b>ວັນທີ່:
                                                        @if (!empty($starts))
                                                            {{ date('d-m-Y', strtotime($starts)) }}
                                                        @endif
                                                        ຫາ ວັນທີ່:
                                                        @if (!empty($ends))
                                                            {{ date('d-m-Y', strtotime($ends)) }}
                                                        @endif
                                                    </b></h4>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <table class="table table-striped table-hover">
                                                    <thead>
                                                        <tr class="text-center bg-gradient-pink text-bold">
                                                            <th>ລຳດັບ</th>
                                                            <th>ວັນທີ່ນຳເຂົ້າ</th>
                                                            <th>ລະຫັດ</th>
                                                            <th>ສິນຄ້າ</th>
                                                            <th>ປະເພດສິນຄ້າ</th>
                                                            <th>ຫົວຫນ່ວຍ</th>
                                                            <th>ລາຄາຊື້</th>
                                                            <th>ສະຕ໋ອກ</th>
                                                            <th>ສະຖານະ</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php $no = 1 @endphp
                                                        @foreach ($products as $item)
                                                            <tr class="text-center">
                                                                <td>{{ $no++ }}</td>
                                                                <td>{{ date('d/m/Y', strtotime($item->updated_at)) }}
                                                                </td>
                                                                <td>
                                                                     {{ $item->code }}
                                                                </td>
                                                                <td>
                                                                    {{ $item->name }}
                                                               </td>
                                                                <td class="text-bold">
                                                                    @if (!empty($item->product_type))
                                                                        {{ $item->product_type->name }}
                                                                    @endif
                                                                </td>
                                                                <td class="text-bold">
                                                                    @if (!empty($item->unit))
                                                                        {{ $item->unit->name }}
                                                                    @endif
                                                                </td>
                                                                <td class="text-bold">{{ number_format($item->buy_price) }} ₭</td>
                                                                <td><p class="bg-success rounded">{{ $item->stock }}</p> </td>
                                                                <td>
                                                                    @if ($item->stock <= 0)
                                                                    <p class="text-danger text-center">
                                                                        ສິນຄ້າຫມົດເເລ້ວ</p>
                                                                    @elseif($item->stock > 10)
                                                                        <p class="text-success text-center">
                                                                            ຍັງມີໃນສະຕ໋ອກ</p>
                                                                    @elseif($item->stock <= 10)
                                                                    <p class="text-warning">
                                                                        ກຳລັງສິຫມົດ!</p>
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- end card body-->
                    </div><!-- end card -->
                </div>
            </div>
        </div>
    </section>
</div>
@push('scripts')
    <script>
        $(document).ready(function() {
            $('#print').click(function() {
                printDiv();

                function printDiv() {
                    var printContents = $(".right_content").html();
                    var originalContents = document.body.innerHTML;
                    document.body.innerHTML = printContents;
                    window.print();
                    document.body.innerHTML = originalContents;
                }
                location.reload();
            });
        });
    </script>
@endpush
