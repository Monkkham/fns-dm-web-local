            <!--Foram add new-->
            <div class="col-md-4">
                <div class="card card-default">
                  <div class="card-header bg-pink">
                    <label>ເພີ່ມໃຫມ່ / ແກ້ໄຂ</label>
                  </div>
                  <form>
                      <div class="card-body">
                          <input type="hidden" wire:model="ID" value="{{$ID}}">
                          <div class="row">
                            <div class="col-md-12">
                              <div class="form-group" wire:ignore>
                                  <label>ເລືອກປະເພດອາຫານ</label>
                                  <select wire:model="foods_type_id" class="form-control" id="select_fill">
                                    <option value="" selected>ເລືອກ</option>
                                      @foreach($foods_type as $item)
                                          <option value="{{ $item->id }}">{{ $item->name }}</option>
                                      @endforeach
                                  </select>
                                  @error('foods_type_id') <span style="color: red" class="error">{{ $message }}</span> @enderror
                              </div>
                          </div>
                              <div class="col-md-12">
                                <div class="form-group">
                                  <label>ຮູບພາບ</label>
                                  <input wire:model="image" type="file" class="form-control @error('image') is-invalid @enderror" placeholder="ປ້ອນຂໍ້ມູນ">
                                  @error('image') <span style="color: red" class="error">{{ $message }}</span> @enderror
                                </div>
                              </div>
                              <div class="col-md-12">
                                <div class="form-group">
                                  <label>ຊື່</label>
                                  <input wire:model="name" type="text" class="form-control @error('name') is-invalid @enderror" placeholder="ປ້ອນຂໍ້ມູນ">
                                  @error('name') <span style="color: red" class="error">{{ $message }}</span> @enderror
                                </div>
                              </div>
                              <div class="col-md-12">
                                <div class="form-group">
                                  <label>ລາຄາ</label>
                                  <input wire:model="price" type="number" min="1" class="form-control @error('price') is-invalid @enderror" placeholder="ປ້ອນຂໍ້ມູນ">
                                  @error('price') <span style="color: red" class="error">{{ $message }}</span> @enderror
                                </div>
                              </div>
                          </div>
                      </div>
                      <div class="card-footer">
                          <div class="d-flex justify-content-between md-2">
                            @foreach ($rolepermissions as $items)
                            @if ($items->permissionname->name == 'action_food')
                                  <button type="button" wire:click="resetform" class="btn btn-warning">ຣີເຊັດ</button>
                                  <button type="button" wire:click="store" class="btn btn-success">ບັນທຶກ</button>
                                  @endif
                              @endforeach
                          </div>
                      </div>
                  </form>
                </div>
              </div>

@push('scripts')
    <script>
      window.addEventListener('show-modal-delete', event => {
          $('#modal-delete').modal('show');
      })
      window.addEventListener('hide-modal-delete', event => {
          $('#modal-delete').modal('hide');
      })
    </script>
@endpush