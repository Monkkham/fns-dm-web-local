<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PreOrderFood extends Model
{
    use HasFactory;
    protected $table = "preorder_food";
    protected $fillable = ['id',
    'preorder_id',
    'foods_id',
    'food_qty', //ຈຳນວນອາຫານທີ່ຈອງ
    'subtotal',
    'created_at',
    'updated_at'];
    public function preorder()
    {
        return $this->belongsTo('App\Models\PreOrder','preorder_id','id');
    }
    public function foods()
    {
        return $this->belongsTo('App\Models\Foods','foods_id','id');
    }
}
