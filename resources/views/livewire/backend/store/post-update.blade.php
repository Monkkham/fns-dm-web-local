    <!-- /.modal-edit -->
    <div wire:ignore.self class="modal fade" id="modal-edit">
        <div class="modal-dialog modal-gl">
            <div class="modal-content">
                <div class="modal-header bg-light">
                    <h5 class="modal-title"><i class="fa fa-edit text-success"></i> ແກ້ໄຂ</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="container">
                            <div class="avatar-upload">
                                <div class="avatar-edit">
                                    <input type='file' wire:model="image" id="imageUpload2"
                                        accept=".png, .jpg, .jpeg" />
                                    <label for="imageUpload2"></label>
                                </div>
                                @if ($image)
                                    <div class="avatar-preview">
                                        <img id="imagePreview2" src="{{ $image->temporaryUrl() }}" alt=""
                                            width="120px;">
                                    </div>
                                @else
                                    @if ($newimage)
                                        <div class="avatar-preview">
                                            <img id="imagePreview2" src="{{ asset('post') }}/{{ $newimage }}"
                                                alt="" width="120px;">
                                        </div>
                                    @else
                                        <div class="avatar-preview">
                                            <div id="imagePreview2"
                                                style="background-image: url({{ asset('logo/noimage.jpg') }});">
                                            </div>
                                        </div>
                                    @endif
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="note">ຄຳອະທິບາຍ</label>
                                        <div wire:ignore>
                                            <textarea class="form-control" id="note_update" wire:model="note">{{ $note }}</textarea>
                                        </div>
                                        @error('note')
                                            <span style="color: red" class="error">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                </form>
                <div class="modal-footer justify-content-between bg-light">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">ຍົກເລີກ</button>
                    <button wire:click="update" type="button" class="btn btn-success">ບັນທຶກ</button>
                </div>
            </div>
        </div>
    </div>
