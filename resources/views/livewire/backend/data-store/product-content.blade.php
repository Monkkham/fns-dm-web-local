<div>
    {{-- ======================================== name page ====================================================== --}}
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h6><i class="fas fa-database"></i> ຈັດການຂໍ້ມູນ <i class="fa fa-angle-double-right"></i>
                        ສິນຄ້າ</h6>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">ຫນ້າຫຼັກ</a></li>
                        <li class="breadcrumb-item active">ສິນຄ້າ</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!--List users- table table-bordered table-striped -->
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="row">
                                        <div class="col-md-6">
                                            @foreach ($rolepermissions as $items)
                                            @if ($items->permissionname->name == 'action_product')
                                            <a wire:click="create" class="btn btn-primary btn-sm"
                                                href="javascript:void(0)"><i class="fa fa-plus-circle"></i>
                                                ເພີ່ມໃຫມ່</a>
                                            @endif
                                             @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">

                                </div>
                                <div class="row">
                                    <div class="col-md-9">
                                    </div>
                                    <div class="input-group input-group-sm" style="width: 240px;">
                                        <input wire:model="search" type="text" class="form-control"
                                            placeholder="ຄົ້ນຫາ">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead class="bg-pink text-center">
                                                    <tr>
                                                        <th>ລຳດັບ</th>
                                                        <th>ລະຫັດ</th>
                                                        <th>ຮູບພາບ</th>
                                                        <th>ຊື່</th>
                                                        <th>ລາຄານຳເຂົ້າ</th>
                                                        <th>ສະຕ໋ອກ</th>
                                                        <th>ປະເພດ</th>
                                                        <th>ຫົວຫນ່ວຍ</th>
                                                        <th>ວ.ດ.ປ ນຳເຂົ້າ</th>
                                                        @foreach ($rolepermissions as $items)
                                                        @if ($items->permissionname->name == 'action_product')
                                                        <th>ຈັດການ</th>
                                                        @endif
                                                        @endforeach
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @php
                                                        $num = 1;
                                                    @endphp

                                                    @foreach ($products as $item)
                                                        <tr class="text-center">
                                                            <td>{{ $num++ }}</td>
                                                            <td>{{ $item->code }}</td>
                                                            <td>
                                                                @if (!empty($item->image))
                                                                    <a
                                                                        href="{{ asset('public/'.$item->image) }}">
                                                                        <img class="rounded"
                                                                            src="{{ asset('public/'.$item->image) }}"
                                                                            width="50px;" height="50px;">
                                                                    </a>
                                                                @else
                                                                    <img src="{{ ('logo/noimage.jpg') }}"
                                                                        width="50px;" height="50px;">
                                                                @endif
                                                            </td>
                                                            <td>{{ $item->name }}</td>
                                                            <td>{{ number_format($item->buy_price) }}.00 ₭</td>
                                                            <td><p class="bg-success rounded">{{ number_format($item->stock) }}</p></td>
                                                            @if (!empty($item->product_type))
                                                                <td class="text-success text-bold">{{ $item->product_type->name }}
                                                                </td>
                                                            @else
                                                                <td></td>
                                                            @endif
                                                            @if (!empty($item->unit))
                                                            <td class="text-success text-bold">{{ $item->unit->name }}
                                                            </td>
                                                        @else
                                                            <td></td>
                                                        @endif
                                                            <td>{{ date('d/m/Y', strtotime($item->updated_at)) }}</td>
                                                            @foreach ($rolepermissions as $items)
                                                        @if ($items->permissionname->name == 'action_product')
                                                            <td>
                                                                <div class="btn-group">
                                                                    <button wire:click="edit({{ $item->id }})"
                                                                        type="button" class="btn btn-warning btn-sm"><i
                                                                            class="fa fa-pencil"></i></button>
                                                                    <button
                                                                        wire:click="showDestroy({{ $item->id }})"
                                                                        type="button" class="btn btn-danger btn-sm"><i
                                                                            class="fa fa-trash"></i></button>
                                                                </div>
                                                            </td>
                                                            @endif
                                                        @endforeach
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                            <div class="float-right">
                                                {{ $products->links() }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
    </section>
    @include('livewire.backend.data-store.products-create')
    @include('livewire.backend.data-store.products-update')
    @include('livewire.backend.data-store.products-delete')
</div>
