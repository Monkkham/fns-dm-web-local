  <!-- /.modal-payment -->
  <div wire:ignore.self class="modal fade" id="onepay">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h5 class="modal-title text-center text-white"><i class="fa fa-credit-card"></i> ກວດສອບ OnePay</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span style="font-size:20px" aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="detailpayment">
                    <div class="container-fluid">
                        <div class="row">
                            {{-- @if ($detailpayments->order->image == '0')
              @else --}}
                            <div class="col-md-12">
                                <!-- Profile Image -->
                                <div class="card card-primary card-outline">
                                    <img src="{{ asset('public/'.$onepay_image) }}">
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.card -->
                                <div class="card card-primary">
                                    <div class="card-header">
                                        <h5 class="text-center"><i class="fa fa-compare"></i> ປຽບທຽບຂໍ້ມູນລະບົບ</h5>
                                    </div>
                                    <!-- /.card-header -->
                                    <table class="table table-borderless">
                                        <tr>
                                            <td><i class="fas fa-clock mr-1"></i> ວັນທີ່ຊຳລະ</td>
                                            <td>
                                                {{ date('d/m/Y', strtotime($this->created_at)) }}
                                                {{ date('H:i:s', strtotime($this->created_at)) }}
                                                {{-- @endforeach --}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><i class="fas fa-address-card"></i> ລະຫັດສັ່ງຊື້</td>
                                            <td>
                                                {{-- @foreach ($sales as $item) --}}
                                                {{ $this->code }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><i class="fas fa-money-bill"></i> ລວມເປັນເງິນ</td>
                                            <td>
                                                {{ number_format($this->total) }} ກີບ
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><i class="fa fa-check"></i> ສະຖານະ</td>
                                            <td class="text-success"> ຊໍາລະເງິນແລ້ວ <i class="fa fa-check"></i></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button wire:click="cancle_onepay({{ $ID }})" type="button" class="btn btn-danger"><i class="fa fa-times-circle"></i>
                    ຍົກເລີກ</button>
                <button wire:click="confirm_onepay({{ $ID }})" type="button" class="btn btn-success"><i
                        class="fa fa-check-circle"></i> ຍືນຍັນ</button>
            </div>
        </div>
    </div>
</div>