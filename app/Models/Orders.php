<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    use HasFactory;
    protected $table = "orders";
    protected $fillable = ['id',
    'employee_id',
    'supplier_id',
    'code','total',
    'payment_type',
    'payment', // 1 = ບໍ່ທັນຊຳລະ,  2 = ຊຳລະເເລ້ວ
    'status', // 1 = ລໍຖ້າກວດສອບ,  2 = ນຳເຂົ້າສຳເລັດ 3 ຖືກຍົກເລີີກ
    'note','created_at','updated_at'];
    public function employee()
    {
        return $this->belongsTo('App\Models\Employee','employee_id','id');
    }
    public function supplier()
    {
        return $this->belongsTo('App\Models\Supplier','supplier_id','id');
    }
}
