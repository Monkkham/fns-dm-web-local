<?php

namespace App\Http\Livewire\Backend;

use App\Models\Roles;
use Livewire\Component;
use App\Models\Permission;
use App\Models\RolePermission;

class RoleCreateContent extends Component
{
    public $name,$meaning, $settings, $reports, $users;
    public $selectedtypes = [];
    public function render()
    {
        $permissions = Permission::select('id','name_permission','name')->get();
        return view('livewire.backend.role-create-content',compact('permissions'))->layout('layouts.backend.base');
    }
    public function store()
    {
        $this->validate([
            'name'=>'required'
        ],[
            'name.required'=>'ກະລຸນາເພີ່ມຊື່ສິດການເຂົ້າເຖິງກ່ອນ!'
        ]);

        if(!empty($this->selectedtypes)){
            $role = new Roles();
            $role->name = $this->name;
            $role->meaning = $this->meaning;
            if($this->settings == 'true'){
                $role->settings = $this->settings;
            }
            if($this->reports == 'true'){
                $role->reports = $this->reports;
            }
            if($this->users == 'true'){
                $role->users = $this->users;
            }
            $role->save();
            foreach ($this->selectedtypes as $item)
            {
                $role_permis = new RolePermission();
                $role_permis->role_id = $role->id;
                $role_permis->permissions_id = $item;
                $role_permis->save();
            }
            session()->flash('success', 'ເພີ່ມຂໍ້ມູນສຳເລັດ');
            return redirect(route('backend.role'));
        }else{
            $this->emit('alert', ['type' => 'error', 'message' => 'ກະລຸນາເລືອກຂໍ້ມູນກ່ອນ!']);
        }
    }
    public function back()
    {
        return redirect(route('backend.role'));
    }
}
