<?php

namespace App\Http\Livewire\Backend\PreOrders;
use App\Models\Tables;
use Livewire\Component;
use App\Models\PreOrder;
use App\Models\PreOrderFood;
use Livewire\WithPagination;
use App\Models\PreOrderTable;
use DB;
class PreOrdersContent extends Component
{
    public $code,$customer_id,$name,$total,$payment,$status,$created_at,$onepay_image,$search,$datetime,$qtyp;
    public $ID,$food_data = [],$table_data = [];
    public $count_food_data,$sum_subtotal_food_data,$count_table_data,$customer_data;
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public function render()
    {
        $preorders = PreOrder::whereIn('status',[0,1])
        ->where('code','like','%' . $this->search. '%')
        ->paginate(5);
        return view('livewire.backend.pre-orders.pre-orders-content',compact('preorders'))->layout('layouts.backend.base');
    }
    public function ShowBill($ids)
    {
        $this->dispatchBrowserEvent('show-modal-bill');
        $data = PreOrder::find($ids);
        $this->ID = $data->id;
        $this->customer_data = $data->customer;
        $this->code = $data->code;
        $this->qtyp = $data->qtyp;
        $this->payment = $data->payment;
        $this->status = $data->status;
        $this->datetime = $data->datetime;

        $this->food_data = PreOrderFood::where('preorder_id', $ids)->get();
        $this->count_food_data = PreOrderFood::where('preorder_id', $ids)->count();
        $this->sum_subtotal_food_data = PreOrderFood::where('preorder_id', $ids)->sum('subtotal');

        $this->table_data = PreOrderTable::where('preorder_id', $ids)->get();
        $this->count_table_data = PreOrderTable::where('preorder_id', $ids)->count();
    }
    public function show_onepay($ids)
    {
        $this->dispatchBrowserEvent('show-onepay');
        $data = PreOrder::find($ids);
        $this->ID = $data->id;
        $this->code = $data->code;
        $this->total = $data->total;
        $this->payment = $data->payment;
        $this->created_at = $data->created_at;
        $this->onepay_image = $data->onepay_image;
    }
    public function confirm_onepay()
    {
        $ids = $this->ID;
        $data = PreOrder::find($ids);
        $data->payment = 0;
        $data->save();
        $this->dispatchBrowserEvent('hide-onepay');
        // $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບຂໍ້ມູນສຳເລັດ!']);
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ຍືນຍັນ OnePay ສຳເລັດ!',
            'icon'=>'success',
            'iconColor'=>'green',
        ]);
    }
    public function cancle_onepay()
    {
        $ids = $this->ID;
        $data = PreOrder::find($ids);
        $data->status = 0;
        $data->note = "ໃບບິນຊຳລະເງິນບໍ່ຜ່ານ";
        $data->save();
        $this->dispatchBrowserEvent('hide-onepay');
        // $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບຂໍ້ມູນສຳເລັດ!']);
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ຍົກເລີກລາຍການສັ່ງຈອງເເລ້ວ!',
            'icon'=>'success',
            'iconColor'=>'green',
        ]);
    }
    public function confirm_preorder($ids)
    {
        // $ids = $this->ID;
        $data = PreOrder::find($ids);
        $data->status = 2;
        $data->save();
        $table_status = PreOrderTable::select('*')
        ->where('preorder_id', $ids)
        ->join('tables', 'preorder_table.tables_id', '=', 'tables.id')->update(array('status' => 1));
        $this->dispatchBrowserEvent('hide-onepay');
        // $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບຂໍ້ມູນສຳເລັດ!']);
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ອານຸມັດລາຍການສັ່ງຈອງເເລ້ວ!',
            'icon'=>'success',
            'iconColor'=>'green',
        ]);
    }
}
