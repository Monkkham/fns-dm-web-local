<?php

namespace App\Http\Livewire\Backend;

use App\Models\Roles;
use Livewire\Component;
use Livewire\WithPagination;
use App\Models\RolePermission;

class RoleContent extends Component
{
    protected $paginationTheme = 'bootstrap';
    use WithPagination;
    public $search, $ID;
    public function render()
    {
        $roles = Roles::select('*')
        ->where('name', 'like', '%' . $this->search . '%')
        ->paginate(10);
        return view('livewire.backend.role-content',compact('roles'))->layout('layouts.backend.base');
    }
    public function edit($ids)
    {
        return redirect(route('backend.edit_role',$ids));
    }
    public function showDestroy($ids)
    {
        $this->dispatchBrowserEvent('show-delete');
        $data = Roles::find($ids);
        $this->ID = $data->id;
    }
    public function destroy()
    {
        $ids = $this->ID;
        $data = Roles::find($ids);
        $data->delete();
         $permission = RolePermission::where('role_id', $ids)->get();
         foreach($permission as $item){
             $item->delete();
         }
        $this->dispatchBrowserEvent('hide-delete');
        // $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບຂໍ້ມູນສຳເລັດ!']);
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ລຶບຂໍ້ມູນສຳເລັດ!',
            'icon'=>'success',
            'iconColor'=>'green',
        ]);
    }
}
