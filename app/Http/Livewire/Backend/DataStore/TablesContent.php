<?php

namespace App\Http\Livewire\Backend\DataStore;

use App\Models\Tables;
use App\Models\Zones;
use Carbon\Carbon;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class TablesContent extends Component
{
    use WithFileUploads;
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $ID,
    $zones_id,
    $code,
    $chiar_qty,
    // $image,
    // $newimage,
    $status,
        $search;
    public function render()
    {
        $tables = Tables::orderBy('id', 'desc')
            ->where('code', 'like', '%' . $this->search . '%')
            ->paginate(5);
        $zones = Zones::all();
        return view('livewire.backend.data-store.tables-content', compact('tables', 'zones'))->layout('layouts.backend.base');
    }
    public function resetform()
    {
        $this->zones_id = '';
        $this->code = '';
        $this->chiar_qty = '';
        // $this->image = '';
        $this->ID = '';
    }
    protected $rules = [
        'zones_id' => 'required|unique:tables',
        'chiar_qty' => 'required',
    ];
    protected $messages = [
        'zones_id.required' => 'ກະລຸນາເລືອກຂໍ້ມູນກ່ອນ!',
        'zones_id.unique' => 'ຂໍ້ມູນນີ້ມີໃນລະບົບເເລ້ວ!',
        'chiar_qty.required' => 'ກະລຸນາປ້ອນຂໍ້ມູນກ່ອນ!',
    ];
    // public function updated($propertyzones_id)
    // {
    //     $this->validateOnly($propertyzones_id);
    // }
    public function store()
    {
        $updateId = $this->ID;
        if ($updateId > 0) {
            $this->validate([
                'zones_id' => 'required',
                // 'code'=>'required',
                'chiar_qty' => 'required',
            ], [
                'zones_id.required' => 'ກະລຸນາເລືອກຂໍ້ມູນກ່ອນ!',
                // 'code.required'=>'ກະລຸນາປ້ອນຂໍ້ມູນກ່ອນ!',
                'chiar_qty.required' => 'ກະລຸນາປ້ອນຂໍ້ມູນກ່ອນ!',
            ]);
            $ids = $this->ID;
            $data = Tables::find($ids);
            $data->zones_id = $this->zones_id;
            $data->chiar_qty = $this->chiar_qty;
            // if ($this->image) {
            //     $this->validate([
            //         'image' => 'required|mimes:png,jpg,jpeg',
            //     ]);
            //     if ($this->image) {
            //         $this->validate([
            //             'image' => 'required|mimes:png,jpg,jpeg',
            //         ]);
            //         if ($this->image != $data->image) {
            //             if (!empty($data->image)) {
            //                 $images = explode(",", $data->images);
            //                 foreach ($images as $image) {
            //                     unlink('' . '' . $data->image);
            //                 }
            //                 $data->delete();
            //             }
            //         }
            //         $imageName = Carbon::now()->timestamp . '.' . $this->image->extension();
            //         $this->image->storeAs('upload/tables', $imageName);
            //         $data->image = 'upload/tables' . '/' . $imageName;
            //     }
            // }
            $data->save();
            $this->dispatchBrowserEvent('swal', [
                'title' => 'ແກ້ໄຂຂໍ້ມູນສຳເລັດ !',
                'icon' => 'success',
                'iconColor' => 'green',
            ]);
            $this->resetform();
        } else //ເພີ່ມໃໝ່
        {
            $this->validate([
                'zones_id' => 'required',
                // 'code'=>'required',
                'chiar_qty' => 'required',
            ], [
                'zones_id.required' => 'ກະລຸນາເລືອກຂໍ້ມູນກ່ອນ!',
                // 'code.required'=>'ກະລຸນາປ້ອນຂໍ້ມູນກ່ອນ!',
                'chiar_qty.required' => 'ກະລຸນາປ້ອນຂໍ້ມູນກ່ອນ!',
            ]);
            $employee_max = Tables::count('id');
            $count = $employee_max + 1;
            $data = new Tables();
            if(!empty($employee_max)){
            $data->code = 'TB-0' . $count;
            }else{
                $data->code = 'TB-01';
            }
            $data->zones_id = $this->zones_id;
            $data->chiar_qty = $this->chiar_qty;
            // if (!empty($this->image)) {
            //     $this->validate([
            //         'image' => 'required|mimes:jpg,png,jpeg',
            //     ]);
            //     $imageName = Carbon::now()->timestamp . '.' . $this->image->extension();
            //     $this->image->storeAs('upload/tables', $imageName);
            //     $data->image = 'upload/tables' . '/' . $imageName;
            // } else {
            //     $data->image = '';
            // }
            $data->status = 0;
            $data->save();
            $this->dispatchBrowserEvent('swal', [
                'title' => 'ເພີ່ມຂໍ້ມູນສຳເລັດ !',
                'icon' => 'success',
                'iconColor' => 'green',
            ]);
            $this->resetform();
        }
        // $this->emit('alert', ['type' => 'success', 'message' => 'ເພີ່ມຂໍ້ມູນສຳເລັດ!']);
    }
    public function edit($ids)
    {
        $data = Tables::find($ids);
        $this->zones_id = $data->zones_id;
        $this->chiar_qty = $data->chiar_qty;
        // $this->newimage = $data->image;
        $this->ID = $data->id;
    }
    public function showDestroy($ids)
    {
        $this->dispatchBrowserEvent('show-modal-delete');
        $data = Tables::find($ids);
        $this->ID = $data->id;
        $this->code = $data->code;
    }

    public function destroy($ids)
    {
        $ids = $this->ID;
        $data = Tables::find($ids);
        $data->delete();
        $this->dispatchBrowserEvent('hide-modal-delete');
        // $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບຂໍ້ມູນສຳເລັດ!']);
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ລຶບຂໍ້ມູນສຳເລັດ !',
            'icon' => 'success',
            'iconColor' => 'green',
        ]);
        $this->resetform();
    }
    // public function destroy($ids)
    // {
    //     $ids = $this->ID;
    //     $product_type = Tables::find($ids);
    //         $product_type->del = 0;
    //         $product_type->save();
    //         $this->dispatchBrowserEvent('hide-modal-delete');
    //         $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບຂໍ້ມູນສຳເລັດ!']);
    //         $this->dispatchBrowserEvent('swal', [
    //             'title' => 'ລຶບຂໍ້ມູນສຳເລັດ !',
    //             'icon'=>'success',
    //             'iconColor'=>'green',
    //         ]);
    //         $this->resetdata();
    // }
}
