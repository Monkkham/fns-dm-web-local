<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrdersDetail extends Model
{
    use HasFactory;
    protected $table = "orders_detail";
    protected $fillable = ['id','orders_id','product_id','subtotal','quantity','created_at','updated_at'];
    public function orders()
    {
        return $this->belongsTo('App\Models\Orders','orders_id','id');
    }
    public function products()
    {
        return $this->belongsTo('App\Models\Products','product_id','id');
    }
}
