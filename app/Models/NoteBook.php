<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NoteBook extends Model
{
    use HasFactory;
    use HasFactory;
    protected $table = "notebook";
    protected $fillable = ['id','code','type','name','money','created_at','updated_at'];
}
