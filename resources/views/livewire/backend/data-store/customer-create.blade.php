    <!-- /.modal-add -->
    <div wire:ignore.self class="modal fade" id="modal-add">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header bg-light">
                    <h5 class="modal-title"><i class="fa fa-plus text-success"></i> ເພີ່ມໃຫມ່</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="container">
                            <div wire:ignore class="avatar-upload">
                                <div class="avatar-edit">
                                    <input type='file' wire:model="image" id="imageUpload"
                                        accept=".png, .jpg, .jpeg" />

                                    <label for="imageUpload"></label>
                                </div>
                                <label class="text-center">ໃສ່ຮູບພາບ(ຖ້າມີ)</label>
                                <div class="avatar-preview">
                                    <div id="imagePreview"
                                        style="background-image: url({{ asset('logo/noimage.png') }});">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <label>ເລືອກເພດ</label>
                                <div class="form-group clearfix">
                                    <div class="icheck-success d-inline">
                                        <input type="radio" id="radioPrimary1" value="1" wire:model="gender"
                                            checked>
                                        <label for="radioPrimary1">ຍິງ
                                        </label>
                                    </div>
                                    {{-- </div>
                                <div class="form-group clearfix"> --}}
                                    <div class="icheck-success d-inline">
                                        <input type="radio" id="radioPrimary2" value="2" wire:model="gender">
                                        <label for="radioPrimary2">ຊາຍ
                                        </label>
                                    </div>
                                </div>
                                @error('gender')
                                    <span style="color: red" class="error">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>ຊື່</label>
                                    <input wire:model="name" type="text" placeholder="ປ້ອນຂໍ້ມູນ"
                                        class="form-control @error('name') is-invalid @enderror">
                                    @error('name')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>ນາມສະກຸນ</label>
                                    <input wire:model="lastname" type="text" placeholder="ປ້ອນຂໍ້ມູນ"
                                        class="form-control @error('lastname') is-invalid @enderror">
                                    @error('lastname')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>ເບີໂທ</label>
                                    <input wire:model="phone" type="text" placeholder="ປ້ອນຂໍ້ມູນ"
                                        class="form-control @error('phone') is-invalid @enderror">
                                    @error('phone')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>ອີເມວ</label>
                                    <input wire:model="email" type="text" placeholder="ປ້ອນຂໍ້ມູນ"
                                        class="form-control @error('email') is-invalid @enderror">
                                    @error('email')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>ທີ່ຢູ່ປະຈຸບັນ</label>
                                    <input wire:model="address" type="text" placeholder="ປ້ອນຂໍ້ມູນ"
                                        class="form-control @error('address') is-invalid @enderror">
                                    @error('address')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            {{-- <div class="col-md-3">
                                <div class="form-group">
                                    <label>ເລືອກ{{ __('lang.type') }}{{ __('lang.customers') }}</label>
                                    <select wire:model="customer_type_id" class="form-control">
                                        <option value="" selected>ເລືອກ</option>
                                        @foreach ($customer_type as $item)
                                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('customer_type_id')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div> --}}
                            {{-- <div class="col-md-3">
                                <div class="form-group">
                                    <label>ເລືອກສິດທິ</label>
                                    <select wire:model="roles_id" class="form-control">
                                        <option value="" selected>ເລືອກ</option>
                                        @foreach ($roles as $item)
                                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('roles_id')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div> --}}
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>ລະຫັດຜ່ານ</label>
                                    <input wire:model="password" type="password" id="password" placeholder="********"
                                        class="form-control @error('address') is-invalid @enderror">
                                        @error('password')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                    <input type="checkbox" id="eye"
                                        onclick="show_password()">&nbsp;<label>ສະເເດງ</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>ຍືນຍັນລະຫັດຜ່ານ</label>
                                    <input wire:model="con_password" type="password" id="con_password"
                                        placeholder="********" class="form-control">
                                </div>
                                {{-- @if (session::has('no_match_password'))
                                    <span style="color: red" class="error">{{ session::get('no_match_password') }}</span>
                                @endif --}}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group" wire:ignore>
                                    <label>ເລືອກແຂວງ</label>
                                    <select wire:model="province_id" id="select1" class="form-control
                                     @error('province_id') is-invalid @enderror">
                                        <option value="" selected>ເລືອກ</option>
                                        @foreach ($province as $item)
                                            <option value="{{ $item->id }}">{{ $item->name_la }}</option>
                                        @endforeach
                                    </select>
                                    @error('province_id')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>ເລືອກເມືອງ</label>
                                    <select wire:model="district_id" id="select2" class="form-control
                                     @error('district_id') is-invalid @enderror">
                                        <option value="" selected>ເລືອກ</option>
                                        @foreach ($district as $item)
                                            <option value="{{ $item->id }}">{{ $item->name_la }}</option>
                                        @endforeach
                                    </select>
                                    @error('district_id')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>ເລືອກບ້ານ</label>
                                    <select wire:model="village_id" id="select3" class="form-control
                                     @error('village_id') is-invalid @enderror">
                                        <option value="" selected>ເລືອກ</option>
                                        @foreach ($village as $item)
                                            <option value="{{ $item->id }}">{{ $item->name_la }}</option>
                                        @endforeach
                                    </select>
                                    @error('village_id')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                </div>
                </form>
                <div class="modal-footer justify-content-between bg-light">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">ຍົກເລີກ</button>
                    <button wire:click="store" type="button" class="btn btn-success">ບັນທຶກ</button>
                </div>
            </div>
        </div>
    </div>
