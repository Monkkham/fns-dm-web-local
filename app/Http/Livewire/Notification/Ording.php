<?php

namespace App\Http\Livewire\Notification;

use App\Models\PreOrder;
use Livewire\Component;

class Ording extends Component
{
    public $countPreOrder,$countPreOrderGet;
    public function render()
    {
        $this->countPreOrderGet = PreOrder::where('status', 1)->get();
        $this->countPreOrder = PreOrder::where('status', 1)->count();
        return view('livewire.notification.ording');
    }
}