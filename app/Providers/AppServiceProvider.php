<?php

namespace App\Providers;

use View;
use App\Models\Roles;
use App\Models\PreOrder;
use App\Models\ContactUs;
use App\Models\RolePermission;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('*', function($view)
     {
        if (Auth::check()){
          //  $user_id_shared = Auth::user()->id;
           $roles = Roles::find(auth()->user()->roles_id);
           $rolepermissions = RolePermission::where('role_id', auth()->user()->roles_id)->orderBy('id','desc')->get();
           $contacts = ContactUs::all();
           $contacts_count = ContactUs::count('id');
           $preorder = PreOrder::where('status',1)->get();
           $preorder_count = PreOrder::where('status',1)->count('id');
           View::share([
            'preorder'=>$preorder,
            'preorder_count'=>$preorder_count,
            'contacts'=>$contacts,
            'contacts_count'=>$contacts_count,
            'rolepermissions'=> $rolepermissions,'roles'=> $roles]);
     }
    });
  }
}
