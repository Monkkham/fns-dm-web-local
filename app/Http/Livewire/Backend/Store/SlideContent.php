<?php

namespace App\Http\Livewire\Backend\Store;

use Livewire\Component;
use App\Models\SlidePhoto;
use Livewire\WithPagination;
use Livewire\WithFileUploads;
use Carbon\Carbon;

class SlideContent extends Component
{
    use WithFileUploads;
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $name,$image,$newimage,$search,$ID;
    public function render()
    {
        $slide = SlidePhoto::orderBy('id','desc')
        ->where('name','like','%' . $this->search. '%')
        ->paginate(5);
        return view('livewire.backend.store.slide-content',compact('slide'))->layout('layouts.backend.base');
    }
    public function resetform()
    {
        $this->image = '';
        $this->name = '';
    }
    public function create()
    {
        $this->resetform();
        $this->dispatchBrowserEvent('show-modal-add');
    }

    public function store()
    {
        $this->validate([
            'name'=>'required',
            // 'image'=>'required',
        ],[
            'name.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            // 'image.required'=>'ເລືອກຮູບພາບກ່ອນ!',
        ]);
        $data = new SlidePhoto();
            //upload image
            if (!empty($this->image)) {
                $this->validate([
                    'image' => 'required|mimes:jpg,png,jpeg',
                ]);
                $imageName = Carbon::now()->timestamp . '.' . $this->image->extension();
                $this->image->storeAs('upload/slide', $imageName);
                $data->image = 'upload/slide'.'/'.$imageName;
            }else{
                $data->image = '';
            }
        $data->name = $this->name;
        $data->save();
        $this->dispatchBrowserEvent('hide-modal-add');
        // $this->emit('alert', ['type' => 'success', 'message' => 'ບັນທຶກຂໍ້ມູນສຳເລັດ!']);
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ບັນທຶກຂໍ້ມູນສຳເລັດ!',
            'icon'=>'success',
            'iconColor'=>'green',
        ]);
        $this->resetform();
}
    public function edit($ids)
    {
        $this->dispatchBrowserEvent('show-modal-edit');

        $Data = SlidePhoto::find($ids);
        $this->ID = $Data->id;
        $this->newimage = $Data->image;
        $this->name = $Data->name;
    }
    public function update()
    {
        // $this->validate([
        //     'name'=>'required',
        //     'image'=>'required',
        // ],[
        //     'name.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
        //     'image.required'=>'ເລືອກຮູບພາບກ່ອນ!',
        // ]);
        $ids = $this->ID;
        $data = SlidePhoto::find($ids);
        $data->name = $this->name;
        if ($this->image) {
            $this->validate([
                'image' => 'required|mimes:png,jpg,jpeg',
            ]);
            if ($this->image) {
                $this->validate([
                    'image' => 'required|mimes:png,jpg,jpeg',
                ]);
                if ($this->image != $data->image) {
                    if (!empty($data->image)) {
                        $images = explode(",", $data->images);
                        foreach ($images as $image) {
                            unlink('' . '' . $data->image);
                        }
                        $data->delete();
                    }
                }
                $imageName = Carbon::now()->timestamp . '.' . $this->image->extension();
                $this->image->storeAs('upload/slide', $imageName);
                $data->image = 'upload/slide'.'/'.$imageName;
            }
        }
        $data->save();
        $this->dispatchBrowserEvent('hide-modal-edit');
        // $this->emit('alert', ['type' => 'success', 'message' => 'ແກ້ໄຂຂໍ້ມູນສຳເລັດ!']);
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ແກ້ໄຂຂໍ້ມູນສຳເລັດ!',
            'icon'=>'success',
            'iconColor'=>'green',
        ]);
        $this->resetform();
}
public function showDestroy($ids)
{
    $this->dispatchBrowserEvent('show-modal-delete');
    $Data = SlidePhoto::find($ids);
    $this->ID = $Data->id;
    $this->name = $Data->name;
}
public function destroy()
{
    $ids = $this->ID;
    $data = SlidePhoto::find($ids);
    $data->delete();
    $this->dispatchBrowserEvent('hide-modal-delete');
    // $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບຂໍ້ມູນສຳເລັດ!']);
    $this->dispatchBrowserEvent('swal', [
        'title' => 'ລຶບຂໍ້ມູນສຳເລັດ!',
        'icon'=>'success',
        'iconColor'=>'green',
    ]);
}
}
