<!-- Main content -->
<div wire:ignore.self class="modal fade" id="modal-bill-customer">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-light">
                <h5 class="modal-title"> <b>
                        <button type="button" id="print"
                            class="btn btn-info btn-sm"><i class="fas fa-print"></i> ປິ່ຣນ
                        </button>
                    </b></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body right_content">
                <div class="invoice p-4 mb-4">
                    <div class="row">
                            <div class="col-md-12 invoice-col">
                                <small>
                                    <p class="brand-link">
                                        <img src="{{ asset('logo/logo.jpg') }}" alt="AdminLTE Logo"
                                            class="brand-image img-circle elevation-3" style="opacity: .8"> ຮ້ານ ອາຫານ
                                        ດາວອັງຄານ
                                    <p><small>ບ້ານ ນາທົ່ມ, ເມືອງໄຊທານີ, ນະຄອນຫລວງວຽງຈັນ <br>ໂທ: 020 96378460</small></p>
                                    </p>

                                </small>
                            </div>
                    </div>
                        <div class="row">
                        <div class="invoice-info">
                            <div class="col-md-12">
                                <h6>
                                    <small class="float-right text-sm">
                                        <p>ເລກທີ່ບິນ: {{ $this->code }}</p>
                                        <b>
                                            <i class="fas fa-calendar-alt"></i> ວັນທີ່:</b>
                                        {{ date('d/m/Y', strtotime($this->datetime)) }}
                                        {{ date('H:i:s', strtotime($this->datetime)) }}
                                    </small>
                                </h6>
                            </div>
                        </div>
                        <div class="invoice-info">
                            <div class="col-md-12">
                                <h6>
                                    <small class="float-right text-sm">
                                        <p>ໂຕະ ເຄົາເຕີ</p>
                                        <p><b>ຜູ້ອອກບິນ:</b> {{ Auth::guard('admin')->user()->name }}</p>
                                    </small>
                                </h6>
                            </div>
                        </div>
                        <div class="invoice-info">
                            <div class="col-md-12">
                                <h6>
                                    <small class="float-right text-sm">
                                        <p><small>
                                            ເບີໂຕະ:
                                            @foreach ($this->table_data as $item)
                                            @if(!empty($item->tables))
                                            {{ $item->tables->zones->name }}:{{ $item->tables->code }}
                                            @endif
                                            @endforeach
                                        </small>
                                            </p>
                                        <p>
                                            <b>ລູກຄ້າ: </b>
                                            @if(!empty($this->customer_data))
                                            {{ $this->customer_data->name }}
                                            @else
                                            ທົ່ວໄປ
                                            @endif
                                        </p>
                                    </small>
                                </h6>
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>

                    <div class="row">
                        <div class="col-12 table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <td colspan="6" class="text-center">
                                            <h5 class="text-bold">ບິນເກັບເງິນ</h5>
                                        </td>
                                    </tr>
                                    <tr class="bg-light text-sm">
                                        <th>ລຳດັບ</th>
                                        <th>ລາຍການ</th>
                                        <th>ລາຄາ</th>
                                        <th>ຈຳນວນ</th>
                                        <th>ເປັນເງິນ</th>
                                        <th>ເພີ່ມ</th>
                                    </tr>
                                </thead>
                                @php
                                    $num = 1;
                                @endphp
                                <tbody>
                                    @foreach ($this->food_data as $item)
                                        @if (!empty($item->foods))
                                            <tr class="text-sm">
                                                <td>{{ $num++ }}</td>
                                                <td class="text-bold">{{ $item->foods->name }}</td>
                                                <td>{{ number_format($item->foods->price) }} ₭</td>
                                                <td>{{ $item->food_qty }}</td>
                                                <td>{{ number_format($item->foods->price * $item->food_qty) }} ₭</td>
                                                <td>
                                                   @if($item->add_more == 1)
                                                    <p class="text-danger"><small>ສັ່ງເພີ່ມ <span
                                                        class="spinner-grow mb-1 spinner-grow-sm text-white text-center"
                                                        role="status" aria-hidden="true"></span></small></p>
                                                   @endif
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                    <tr>
                                        <td colspan="3"></td>
                                        <td class="text-bold"><i>ລວມເປັນເງິນ:</i></td>
                                        <td class="text-bold">{{ number_format($this->sum_subtotal_food_data) }} ₭</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.col -->
                    </div>
            </div>
        </div>
        <div class="modal-footer justify-content-right">
            {{-- <button wire:click="cancle_onepay({{ $ID }})" type="button" class="btn btn-danger"><i class="fa fa-times-circle"></i>
                ຍົກເລີກ</button> --}}
            <button wire:click="confirm_add_more({{ $ID }})" type="button" class="btn btn-success"><i
                class="fa fa-check-circle"></i> ຍືນຍັນ</button>
        </div>
        </div>
    </div>
</div>
<!-- /.invoice -->
