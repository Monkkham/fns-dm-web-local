<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Villages extends Model
{
    use HasFactory;
    protected $table = 'village';
    protected $fillable = ['id','name_la','name_en','district_id','created_at','updated_at'];

    public function district()
    {
        return $this->belongsTo('App\Models\Districts','district_id','id');
    }
}
