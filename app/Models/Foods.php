<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Foods extends Model
{
    use HasFactory;
    protected $table = "foods";
    protected $fillable = [
        'id',
        'foods_type_id',
        'code',
        'name',
        'price',
        'status', // 0 = ປິດເມນູອາຫານ , 1 = ເປີດເມນູອາຫານ
        'add_more', // 0 = ສັ່ງປົກກະຕິ , 1 = ສັ່ງເພີ່ມເຕີມ ເວລາຍືນຍັນບິນເມນູຈະກາຍເປັນ 0
        'created_at',
        'updated_at'];
    public function foods_type()
    {
        return $this->belongsTo('App\Models\FoodsType','foods_type_id','id');
    }
    public function products()
    {
        return $this->belongsTo('App\Models\Products','products_id','id');
    }
}
