<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    use HasFactory;
    protected $table = "products";
    protected $fillable = ['id','product_type_id','code','name','buy_price','stock','image','note','created_at','updated_at'];
    public function product_type()
    {
        return $this->belongsTo('App\Models\ProductType','product_type_id','id');
    }
    public function unit()
    {
        return $this->belongsTo('App\Models\Units','unit_id','id');
    }
}
