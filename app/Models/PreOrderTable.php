<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PreOrderTable extends Model
{
    use HasFactory;
    protected $table = "preorder_table";
    protected $fillable = ['id',
    'preorder_id',
    'tables_id',
    'created_at',
    'updated_at'];
    public function preorder()
    {
        return $this->belongsTo('App\Models\PreOrder','preorder_id','id');
    }
    public function tables()
    {
        return $this->belongsTo('App\Models\Tables','tables_id','id');
    }
}
