<div>
    {{-- ======================================== name page ====================================================== --}}
    <div class="right_col" role="main">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h6><i class="fa fa-database"></i> ຈັດການຂໍ້ມູນ <i class="fa fa-angle-double-right"></i>
                        ໂຊນຮ້ານ</h6>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">ຫນ້າຫຼັກ</a></li>
                        <li class="breadcrumb-item active">ໂຊນຮ້ານ</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                {{-- ======================================== show and seach data ====================================================== --}}
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header bg-pink">
                            <div class="row">
                                <div class="col-md-8">
                                </div>
                                <div class="input-group input-group-sm" style="width: 200px;">
                                    <input wire:model="search" type="text" class="form-control"
                                        placeholder="ຄົ້ນຫາ...">

                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-borderless">
                                    <thead class="bg-light">
                                        <tr class="text-center">
                                            <th>ລຳດັບ</th>
                                            <th>ລະຫັດ</th>
                                            <th>ຊື່</th>
                                            @foreach ($rolepermissions as $items)
                                                @if ($items->permissionname->name == 'action_zone')
                                            <th>ຈັດການ</th>
                                            @endif
                                            @endforeach
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $num = 1;
                                        @endphp
                                        @foreach ($zones as $item)
                                            <tr class="text-center">
                                                <td>{{ $num++ }}</td>
                                                <td>{{ $item->code }}</td>
                                                <td>{{ $item->name }}</td>
                                                @foreach ($rolepermissions as $items)
                                                @if ($items->permissionname->name == 'action_zone')
                                                <td>
                                                    <div class="btn-group">
                                                        <button wire:click="edit({{ $item->id }})" type="button"
                                                            class="btn btn-warning btn-sm"><i
                                                                class="fa fa-pencil"></i></button>
                                                        <button wire:click="showDestroy({{ $item->id }})"
                                                            type="button" class="btn btn-danger btn-sm"><i
                                                                class="fa fa-trash"></i></button>
                                                    </div>
                                                    @endif
                                                        @endforeach
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <div class="float-right">
                                    {{ $zones->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @include('livewire.backend.data-store.zones-create-update')
            </div>
        </div>
    </section>
    {{-- ======================================== modal-delete ====================================================== --}}
    <div class="modal fade" id="modal-delete">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h4 class="modal-title"><i class="fa fa-trash text-white"></i></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <input type="hidden" wire:model="ID">
                    <h4 class="text-center">ທ່ານຕ້ອງການລຶບ <b>({{ $name }})</b> ບໍ່?</h4>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">ຍົກເລີກ</button>
                    <button wire:click="destroy({{ $ID }})" type="button"
                        class="btn btn-success">ຕົກລົງ</button>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
