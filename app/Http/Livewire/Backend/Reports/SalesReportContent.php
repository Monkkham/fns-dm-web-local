<?php

namespace App\Http\Livewire\Backend\Reports;

use App\Models\PreOrder;
use App\Models\PreOrderFood;
use App\Models\PreOrderTable;
use Livewire\Component;

class SalesReportContent extends Component
{
    public $generate_Number;
    public $start, $end;
    public $starts, $ends;
    public $code, $customer_id, $name, $total, $payment, $status, $created_at, $onepay_image, $search, $datetime, $qtyp;
    public $ID, $food_data = [], $table_data = [];
    public $count_food_data, $sum_subtotal_food_data, $count_table_data, $customer_data;
    public function render()
    {
        $this->generate_Number = rand(10000000,99999999);
        $start = $this->starts;
        $end = date('Y-m-d', strtotime($this->ends . ' + ' . 1 . 'days'));
        $sales = PreOrder::select('*')
            ->where('status', 3)
            ->whereBetween('created_at', [$this->starts, $end])->get();
        $sum_total = PreOrder::select('total')
            ->where('status', 3)
            ->whereBetween('created_at', [$this->starts, $end])
            ->orderBy('id', 'desc')->sum('total');
        $sum_people = PreOrder::select('qtyp')
            ->where('status', 3)
            ->whereBetween('created_at', [$this->starts, $end])
            ->orderBy('id', 'desc')->sum('qtyp');
        return view('livewire.backend.reports.sales-report-content', compact('sales', 'sum_total', 'sum_people'))->layout('layouts.backend.base');
    }
    public function ShowDetail($ids)
    {
        $this->dispatchBrowserEvent('show-modal-detail');
        $data = PreOrder::find($ids);
        $this->ID = $data->id;
        $this->customer_data = $data->customer;
        $this->code = $data->code;
        $this->qtyp = $data->qtyp;
        $this->payment = $data->payment;
        $this->status = $data->status;
        $this->datetime = $data->datetime;

        $this->food_data = PreOrderFood::where('preorder_id', $ids)->get();
        $this->count_food_data = PreOrderFood::where('preorder_id', $ids)->count();
        $this->sum_subtotal_food_data = PreOrderFood::where('preorder_id', $ids)->sum('subtotal');

        $this->table_data = PreOrderTable::where('preorder_id', $ids)->get();
        $this->count_table_data = PreOrderTable::where('preorder_id', $ids)->count();
    }
    public function sub()
    {
        if (!empty($this->start || $this->end)) {
            $this->starts = $this->start;
            $this->ends = $this->end;
        } else {
            $this->emit('alert', ['type' => 'error', 'message' => 'ກະລຸນາເລືອກວັນທີ່ຫາວັນທີ່ກ່ອນ!']);
        }
    }
}
