<?php

namespace App\Http\Livewire\Backend\DataStore;

use Carbon\Carbon;
use App\Models\Foods;
use Livewire\Component;
use App\Models\Products;
use App\Models\FoodsType;
use Livewire\WithPagination;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Storage;

class FoodContent extends Component
{
    use WithFileUploads;
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $ID,
    $foods_type_id,
    $code,
    $image,
    $name,
    $price,
    $status,
    $imageUrl,
    $newimage,
        $search;
    public function render()
    {
        $foods = Foods::orderBy('id', 'desc')
            ->where('code', 'like', '%' . $this->search . '%')
            ->paginate(5);
        $foods_type = FoodsType::all();
        $products = Products::all();
        return view('livewire.backend.data-store.food-content', compact('foods', 'foods_type', 'products'))->layout('layouts.backend.base');
    }
    public function CloseMenu($id)
    {
        $data = Foods::find($id);
        $data->status = 1;
        $data->save();
        $this->dispatchBrowserEvent('hide-modal-delete');
        // $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບຂໍ້ມູນສຳເລັດ!']);
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ເປີດເມນູສຳເລັດ !',
            'icon' => 'success',
            'iconColor' => 'green',
        ]);
    }
    public function OpenMenu($id)
    {
        $data = Foods::find($id);
        $data->status = 0;
        $data->save();
        $this->dispatchBrowserEvent('hide-modal-delete');
        // $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບຂໍ້ມູນສຳເລັດ!']);
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ປີດເມນູສຳເລັດ !',
            'icon' => 'success',
            'iconColor' => 'green',
        ]);
    }
    public function resetform()
    {
        $this->foods_type_id = '';
        $this->code = '';
        $this->image = '';
        $this->name = '';
        $this->price = '';
        $this->ID = '';
    }
    protected $rules = [
        'foods_type_id' => 'required|unique:foods',
        'name' => 'required',
    ];
    protected $messages = [
        'foods_type_id.required' => 'ກະລຸນາເລືອກຂໍ້ມູນກ່ອນ!',
        'foods_type_id.unique' => 'ຂໍ້ມູນນີ້ມີໃນລະບົບເເລ້ວ!',
        'name.required' => 'ກະລຸນາປ້ອນຂໍ້ມູນກ່ອນ!',
    ];
    // public function updated($propertyfoods_type_id)
    // {
    //     $this->validateOnly($propertyfoods_type_id);
    // }
    public function store()
    {
        $updateId = $this->ID;
        if ($updateId > 0) {
            $this->validate([
                'foods_type_id' => 'required',
                // 'code'=>'required',
                'name' => 'required',
            ], [
                'foods_type_id.required' => 'ກະລຸນາເລືອກຂໍ້ມູນກ່ອນ!',
                // 'code.required'=>'ກະລຸນາປ້ອນຂໍ້ມູນກ່ອນ!',
                'name.required' => 'ກະລຸນາປ້ອນຂໍ້ມູນກ່ອນ!',
            ]);
            $ids = $this->ID;
            $data = Foods::find($ids);
            $data->foods_type_id = $this->foods_type_id;
            $data->name = $this->name;
            $data->price = $this->price;
            if ($this->image) {
                $this->validate([
                    'image' => 'required|mimes:png,jpg,jpeg',
                ]);
                if ($this->image) {
                    $this->validate([
                        'image' => 'required|mimes:png,jpg,jpeg',
                    ]);
                    if ($this->image != $data->image) {
                        if (!empty($data->image)) {
                            $images = explode(",", $data->images);
                            foreach ($images as $image) {
                                unlink('' . '' . $data->image);
                            }
                            $data->delete();
                        }
                    }
                    $imageName = Carbon::now()->timestamp . '.' . $this->image->extension();
                    $this->image->storeAs('public/upload/foods', $imageName);
                    $data->image = 'public/upload/foods' . '/' . $imageName;
                }
            }
            $data->save();
            $this->dispatchBrowserEvent('swal', [
                'title' => 'ແກ້ໄຂຂໍ້ມູນສຳເລັດ !',
                'icon' => 'success',
                'iconColor' => 'green',
            ]);
            $this->resetform();
        } else //ເພີ່ມໃໝ່
        {
            $this->validate([
                'foods_type_id' => 'required',
                // 'code'=>'required',
                'name' => 'required',
                'price' => 'required',
            ], [
                'foods_type_id.required' => 'ກະລຸນາເລືອກຂໍ້ມູນກ່ອນ!',
                // 'code.required'=>'ກະລຸນາປ້ອນຂໍ້ມູນກ່ອນ!',
                'name.required' => 'ກະລຸນາປ້ອນຂໍ້ມູນກ່ອນ!',
                'price.required' => 'ກະລຸນາປ້ອນຂໍ້ມູນກ່ອນ!',
            ]);
            $employee_max = Foods::count('id');
            $count = $employee_max + 1;
            $data = new Foods();
            if (!empty($employee_max)) {
                $data->code = 'FD-00' . $count;
            } else {
                $data->code = 'FD-001';
            }
            $data->foods_type_id = $this->foods_type_id;
            $data->name = $this->name;
            $data->price = $this->price;
            if (!empty($this->image)) {
                $this->validate([
                    'image' => 'required|mimes:jpg,png,jpeg',
                ]);
                $imageName = Carbon::now()->timestamp . '.' . $this->image->extension();
                $this->image->storeAs('public/upload/foods', $imageName);
                $data->image = 'public/upload/foods' . '/' . $imageName;
            } else {
                $data->image = '';
            }
            $data->status = 1;
            $data->save();
            $this->dispatchBrowserEvent('swal', [
                'title' => 'ເພີ່ມຂໍ້ມູນສຳເລັດ !',
                'icon' => 'success',
                'iconColor' => 'green',
            ]);
            $this->resetform();
        }
        // $this->emit('alert', ['type' => 'success', 'message' => 'ເພີ່ມຂໍ້ມູນສຳເລັດ!']);
    }
    public function upload(Request $request)
    {
        $path = $request->file('image')->store('public/images');
        // Get the current environment
        $env = env('APP_ENV');
        DB::table('images')->insert([
            'path' => $path,
            'env' => $env
        ]);
        return back();
    }
    public function edit($ids)
    {
        $data = Foods::find($ids);
        $this->foods_type_id = $data->foods_type_id;
        $this->name = $data->name;
        $this->price = $data->price;
        $this->newimage = $data->image;
        $this->ID = $data->id;
    }
    public function showDestroy($ids)
    {
        $this->dispatchBrowserEvent('show-modal-delete');
        $data = Foods::find($ids);
        $this->ID = $data->id;
        $this->code = $data->code;
    }

    public function destroy($ids)
    {
        $ids = $this->ID;
        $data = Foods::find($ids);
        $data->delete();
        $this->dispatchBrowserEvent('hide-modal-delete');
        // $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບຂໍ້ມູນສຳເລັດ!']);
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ລຶບຂໍ້ມູນສຳເລັດ !',
            'icon' => 'success',
            'iconColor' => 'green',
        ]);
        $this->resetform();
    }
    // public function destroy($ids)
    // {
    //     $ids = $this->ID;
    //     $product_type = Foods::find($ids);
    //         $product_type->del = 0;
    //         $product_type->save();
    //         $this->dispatchBrowserEvent('hide-modal-delete');
    //         $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບຂໍ້ມູນສຳເລັດ!']);
    //         $this->dispatchBrowserEvent('swal', [
    //             'title' => 'ລຶບຂໍ້ມູນສຳເລັດ !',
    //             'icon'=>'success',
    //             'iconColor'=>'green',
    //         ]);
    //         $this->resetdata();
    // }
}
